#include "BufferUtils.h"

Buffer::Buffer()
{
	target = GL_NONE;
	id = 0;
}

void Buffer::store(void* data, unsigned int size, GLenum target, GLenum usage)
{
	glGenBuffers(1, &id);
	glBindBuffer(target, id);
	glBufferData(target, size, data, usage);
	this->target = target;
}

Buffer::Buffer(void* data, unsigned int size, GLenum target, GLenum usage)
{
	glGenBuffers(1, &id);
	glBindBuffer(target, id);
	glBufferData(target, size, data, usage);
	this->target = target;
}

void __thiscall Buffer::bind()
{
	glBindBuffer(target, id);
}

unsigned int Buffer::getId()
{
	return id;
}

vBuffer::vBuffer()
{
	glGenVertexArrays(1, &id);
	attribAmount = 0;
	glBindVertexArray(id);
}

void __thiscall vBuffer::bind()
{
	glBindVertexArray(id);
}

void vBuffer::addAttrib(unsigned int size, GLenum type, unsigned int stride, unsigned int offset)
{
	glVertexAttribPointer(attribAmount, size, type, GL_FALSE, stride, (void*)offset);
	attribAmount++;
}

void vBuffer::enableAttrib()
{
	glEnableVertexAttribArray(attribAmount - 1);
}

void vBuffer::enableAttribId(unsigned int id)
{
	glEnableVertexAttribArray(id);
}

Object3dVaoBuffered::Object3dVaoBuffered(vBuffer* vao, GLenum drawType, unsigned int elementAmount)
{
	vaoBf = vao;
	type = drawType;
	amount = elementAmount;
}

void Object3dVaoBuffered::draw(unsigned int offset)
{
	vaoBf->bind();
	glDrawElements(type, amount, GL_UNSIGNED_INT, (void*)offset);
	glBindVertexArray(0);
}

Transform::Transform(bool rotate, bool scale, bool move)
{
	this->rotate = rotate;
	this->scale = scale;
	this->move = move;
}

glm::mat4 Transform::getMatrix()
{
	glm::mat4 matrix(1.0f);

	if (move == true)
		matrix = glm::translate(matrix, glm::vec3(moveX, moveY, moveZ));
	if (rotate == true)
	{
		matrix = glm::rotate(matrix, rotateX, glm::vec3(1, 0, 0));
		matrix = glm::rotate(matrix, rotateY, glm::vec3(0, 1, 0));
		matrix = glm::rotate(matrix, rotateZ, glm::vec3(0, 0, 1));
	}
	if (scale == true)
		matrix = glm::scale(matrix, glm::vec3(scaleX, scaleY, scaleZ));
	return matrix;
}

void Transform::setMatrix(glm::mat4* matrix, bool reset)
{
	if (reset == true)
		*matrix = glm::mat4(1.0f);
	if (rotate == true)
	{
		*matrix = glm::rotate(*matrix, rotateX, glm::vec3(1, 0, 0));
		*matrix = glm::rotate(*matrix, rotateY, glm::vec3(0, 1, 0));
		*matrix = glm::rotate(*matrix, rotateZ, glm::vec3(0, 0, 1));
	}
	if (move == true)
		*matrix = glm::translate(*matrix, glm::vec3(moveX, moveY, moveZ));
	if (scale == true)
		*matrix = glm::scale(*matrix, glm::vec3(scaleX, scaleY, scaleZ));
}

vector3d<float*> Transform::getRotate()
{
	return vector3d<float*>{&rotateX, & rotateY, & rotateZ};
}

vector3d<float*> Transform::getMove()
{
	return vector3d<float*>{&moveX, &moveY, &moveZ};
}

vector3d<float*> Transform::getScale()
{
	return vector3d<float*>{&scaleX, &scaleY, &scaleZ};
}

void Transform::setRotate(float x, float y, float z)
{
	rotateX = x; rotateY = y; rotateZ = z;
}

void Transform::setMove(float x, float y, float z)
{
	moveX = x; moveY = y; moveZ = z;
}

void Transform::setScale(float x, float y, float z)
{
	scaleX = x; scaleY = y; scaleZ = z;
}

void Transform::appendMove(float x, float y, float z)
{
	moveX += x;
	moveY += y;
	moveZ += z;
}

float Transform::getMoveX()
{
	return moveX;
}

float Transform::getMoveY()
{
	return moveY;
}
