#version 420 core

layout(location = 0) in vec2 pos;
layout(location = 1) in vec2 uv;

out vec2 oUv;

layout (std140, binding = 0) uniform MUBO
{
 mat4 model;
 mat4 PV;
} mubo;

void main()
{
	gl_Position = vec4(vec3(pos.x, 0.0, pos.y), 1.0);
	oUv = uv;
}