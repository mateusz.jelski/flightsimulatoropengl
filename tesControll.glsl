#version 420
#extension GL_ARB_tessellation_shader : enable

uniform float TessLevel;
layout (vertices = 4) out;

in vec2 oUv[];

patch out vec2 c_oUv;
patch out vec3 rPos;

layout (std140, binding = 2) uniform Camera
{
	vec3 cameraPosition;
	mat4 cameraMatrix;
} cam;

layout (std140, binding = 0) uniform MUBO
{
 mat4 model;
 mat4 PV;
} mubo;
#define DISTANCE_DISCARD 170.00

#ifdef DISTANCE_DISCARD
#define TESS_NEAR 8.0
#define TESS_FAR 1.0
#endif

float lerpf(float a, float b, float v)
{
    return a + v * (b - a);
}

void main()
{
	vec4 posmodel = mubo.model * gl_in[gl_InvocationID].gl_Position;
	gl_out[gl_InvocationID].gl_Position = posmodel;
	rPos = gl_in[gl_InvocationID].gl_Position.xyz;
	c_oUv = oUv[gl_InvocationID];


	float tl;
#ifdef DISTANCE_DISCARD
	float d = distance(cam.cameraPosition, posmodel.xyz);
	if (d < DISTANCE_DISCARD)
	{
		tl = lerpf(TESS_NEAR, TESS_FAR, d / DISTANCE_DISCARD);
	}
	else
		tl = -1.0;
#else
	tl = TessLevel;
#endif


	gl_TessLevelInner[0] = tl;
	gl_TessLevelInner[1] = tl;

	gl_TessLevelOuter[0] = tl;
	gl_TessLevelOuter[1] = tl;
	gl_TessLevelOuter[2] = tl;
	gl_TessLevelOuter[3] = tl;
}