#pragma once

#include <iostream>
#include <istream>
#include <string>
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <vector>
#include <ext\vector_float3.hpp>

#define PACKED_DATA_VERTEX 3
#define PACKED_DATA_VERTEX_NORMAL 6
#define PACKED_DATA_VERTEX_NORMAL_TEXTURE 8

typedef unsigned int uint;

void indexLoadToVector(std::vector<float>* vec, std::string* str, int startAt, float* v, float* vt, float* vn, int* fi);
void readFaces3(char bf[16], int* inds);
void readFloat3(char bf[3][16], std::string str, int sp);
void loadObjToList(std::vector<float>* targetArray ,char* path, uint bfSize);
void readIndex3(uint index, float* a, std::vector<float>* vf);
void readIndex2(uint index, float* a, std::vector<float>* vf);

void freeObjMemory(std::vector<float>* v);
void freeObjMemoryFill(std::vector<float>* v);

void getObjectCenter(glm::vec3* min, glm::vec3* max, std::vector<float>* arr, int PACKED_DATA);
