#pragma once
#include "Reader.h"

#include <functional>
#include "BufferUtils.h"
#include <unordered_map>
#include <gtc/matrix_transform.hpp>
#include <vector>

uint compileShader(GLenum type, const char* src);
uint compileProgram(uint vs, uint fs);
uint compileProgram(uint vs, uint gs, uint fs);

void dbgShaderDebugMsg(uint shader, char* bf, uint bfSize);

using namespace glm;

//#define RENDERER_UNBIND

struct _ModelParams
{
	mat4 model;
	bool enabled;
	bool render;
};

enum ATTRIB_TYPE_STORE
{
	STORE_ATTRIB_VERTEX,
	STORE_ATTRIB_VERTEX_TEXTURE,
	STORE_ATTRIB_VERTEX_NORMAL,
	STORE_ATTRIB_VERTEX_TEXTURE_NORMAL,
};

vBuffer* vertexStoreArray(std::vector<float>* v, Buffer* vbo, ATTRIB_TYPE_STORE type);

class Texture2D
{
public:
	Texture2D(const char* path, GLenum mode, GLenum wrap);
	Texture2D(GLenum type, GLenum wrap, int x, int y);
	void bind();
	void unbind();
	void bindImage(uint texIndex, GLenum acces);
	void mipmapgen();
	void clear();
	void clearBuffer();
	char* sample(int x, int y);

	int x, y;
private:
	uint id;
	GLenum type;
	int chn;
	char* data;
};

class UniformBuffer
{
public:
	UniformBuffer(uint size, GLenum mode, uint bindTo);
	void bind();
	void bindBase();
	void bindBaseRanged(uint of_s, uint size);
	void subData(uint offset, uint size, void* d);
	void unbind();

	void bindBaseTo(uint bp);
	uint getBindingPoint();
	void setBindingPoint(uint p);

	uint getID();

	void store(uint size, void* d);


private:
	uint id;
	uint bind_;
};

enum TARGET_RENDER_TYPE
{
	TARGET_RENDER_ARRAY,
	TARGET_RENDER_INDICE
};

enum RENDER_OPT
{
	RENDER_TRUE = 1,
	RENDER_FALSE = 0
};

void texUnitActivate(GLenum e);

class RenderTarget
{
public:
	RenderTarget(vBuffer* vao, Buffer* ebo, int ind, int capacity, Texture2D* tex, TARGET_RENDER_TYPE type);
	RenderTarget(vBuffer* vao, Buffer* ebo, int ind, int capacity, TARGET_RENDER_TYPE type);
	RenderTarget(vBuffer* vao, Buffer* ebo, int ind, int capacity, const char* texPath);
	void addNewObject(mat4 model);
	void addNewObject();

	bool* getRenderLocation(int index);


	void renderClearAll(int set);
	void renderClear(int index, int set);


	mat4* getModel(uint target);
	void setModel(uint target, mat4 m);
	void bindVao();
	void bindEbo();
	void BindTex();
	TARGET_RENDER_TYPE getTargetType();
	void switchTargetType(TARGET_RENDER_TYPE type);

	int getIndice();
	int getTargetCount();

	uint texture;
	std::vector<_ModelParams>* t;
	int count;
private:
	vBuffer* vb;
	Buffer* ebo;
	int indices;
	Texture2D* tex;
	TARGET_RENDER_TYPE type;
};

struct UniformBufferObjectSelector
{
	UniformBuffer* ubo;
	uint offset;
	uint size;
};

class Renderer
{
public:
	Renderer(uint modelL);
	void addTarget(uint id, RenderTarget* tg);
	void removeTrget(uint id);
	void render(uint id);
	void renderTarget(uint id, uint tg);
	mat4* targetMatrix(uint id, uint tgi);
	void targetSetMatrix(uint id, uint tgi, mat4 m);

	void renderCustomBind(uint id, std::function<void(void*, void*)> f);

	void resizeTargetSpaceTo(uint tg, size_t capacity);

	void targetEnable(uint tg, uint tgi);
	void targetDisable(uint tg, uint tgi);
	bool targetGetState(uint tg, uint tgi);

	RenderTarget* getTarget(uint id);
	void setTL(uint tL);



	void renderClearAll(int set);
	void renderClearTragetAll(int set, int target);
	void renderClearTargetSelect(int set, int target, int index);

private:
	std::unordered_map<uint, RenderTarget*> map;
	uint mL;
	uint tL;

	void bindTexture(uint id);
	uint getTL();
};

