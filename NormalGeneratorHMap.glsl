#version 430 core

#define PI 3.141592653589793

layout (local_size_x = 1, local_size_y = 1) in;
layout (r32f, binding = 0) uniform image2D imgMap;
layout (rgba32f, binding = 1) uniform image2D norMap;

layout (std140, binding = 2) uniform IMG_STUFF
{
	ivec2 size;

	float freq;
	float persis;
	float amp;

} imgCtrl;

void main()
{
	ivec2 inv = ivec2(gl_GlobalInvocationID.xy);
	float R = imageLoad(imgMap, inv + ivec2(1, 0)).x;
	float L = imageLoad(imgMap, inv + ivec2(-1, 0)).x;
	float B = imageLoad(imgMap, inv + ivec2(0, -1)).x;
	float T = imageLoad(imgMap, inv + ivec2(0, 1)).x;

	imageStore(imgMap, inv, imageLoad(imgMap, inv)); // * 50.0

	
	imageStore(norMap, inv, vec4(
		normalize(-vec3(
			(L - R), 
			4.0, 
			(B - T)))
				//* 0.25
	, 1.0));
	

}