#include <SDL.h>
#include <glew.h>
#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <string>
#include <fstream>
#include <gtc/matrix_transform.hpp>
#include "Renderer.h"
#include "ObjLoader.h"
#include "TerrainGen.h"

#undef main
#define UP_VECTOR vec3(0.0f, 1.0f, 0.0f)

#define BASIC_DEFINES
#ifdef BASIC_DEFINES
#define SIZEOF_MAT4 64
#define SIZEOF_VEC4 16
#define SIZEOF_VEC3 12
#define SIZEOF_FLOAT 4
#endif

//#define FACE_CULL_ENABLE

struct Plane
{
	vec4 p;
};

struct Frustum
{
	Plane left, right, up, down, near, far;
};

struct v3df
{
	float x, y, z;
};

struct v4df
{
	float x, y, z, w;
};

typedef unsigned char uchar;

template<typename T>
class NodeWrapper
{
public:
	T obj;
};

struct Boundf
{
	float x, y, z, w, h, d;
};

#define prefetch(S) _mm_prefetch((const char*)S, _MM_HINT_T2)

template<typename T>
struct OctreeObject
{
	T object;
	Boundf bound;
};

template<typename T>
struct OctreeLNode
{
	T data;
	OctreeLNode<T>* next;
};

template<typename T>
void appendFront(OctreeLNode<T>** node, T data);

template<typename T>
class OctreeListRef
{
public:
	short amount;
	OctreeLNode<T>* list;
};

template<typename T>
class OctreeNode
{
public:
	OctreeNode();

	//bool canInsert() { return objectPointer == nullptr; }
	bool maskChildrenAnd(uchar m); //{ return maskChildren & m; }
	bool maskObjectsAnd(uchar m); //{ return maskObjects & m; }
	void setHMask(uchar mask); //{ maskChildren = mask; }
	void setOMask(uchar mask); //{ maskObjects = mask; }


	OctreeNode* children[8];
	uchar maskChildren;
	uchar maskObjects;
};

#define MASK_RIGHT_UPPER_FRONT (uchar)1
#define MASK_RIGHT_LOWER_FRONT (uchar)2
#define MASK_LEFT_UPPER_FRONT  (uchar)4
#define MASK_LEFT_LOWER_FRONT  (uchar)8
#define MASK_RIGHT_UPPER_BACK  (uchar)16
#define MASK_RIGHT_LOWER_BACK  (uchar)32
#define MASK_LEFT_UPPER_BACK   (uchar)64
#define MASK_LEFT_LOWER_BACK   (uchar)128

v3df boundPos(Boundf* b);
v3df boundSize(Boundf* b);

typedef int halfSizei;

template<typename T>
struct OctreeTempData
{
	Boundf bound;
	OctreeObject<T>* obj;
};

template<typename T, std::uintmax_t max_depth>
class Octree
{
public:
	Octree(v3df pos, v3df hsize);
	Octree();
	v3df pos; v3df hsize;
	OctreeTempData<T>* OCTD;
	OctreeObject<T>* tempObject;
	int dpth;

	inline bool __fastcall collideBoundf(Boundf tbound, Boundf bound);
	inline bool __fastcall collideBoundfp(Boundf* tbound, Boundf* bound);
	inline bool __fastcall vcollideBoundfp(Boundf* tbound, Boundf* bound);
	inline bool collideBoundfFrustum(Boundf* tbound, Frustum* f);
	void insert(T obj, Boundf bound);
	void collide(Boundf bound);
	void collideFrustum(Frustum* bfp);


private:

	void insertIns(OctreeNode<T>* node);
	void collideIns(OctreeNode<T>* node, Boundf* bfp, int dpth);
	void collideInsFrustum(OctreeNode<T>* node, Frustum* bfp, int dpth_);

	//Node<OctreeObject<T>>* OCObject;
	OctreeNode<T>* OCNode;
};

template<typename T>
inline OctreeNode<T>::OctreeNode()
{
	maskChildren = 0; maskObjects = 0;
}

template<typename T>
inline void OctreeNode<T>::setHMask(uchar mask)
{
	maskChildren = mask;
}

template<typename T>
void appendFront(OctreeLNode<T>** node, T data)
{
	*node = new OctreeLNode<T>{ data, *node };
}

template<typename T>
inline void OctreeNode<T>::setOMask(uchar mask)
{
	maskObjects = mask;
}

template<typename T>
inline bool OctreeNode<T>::maskChildrenAnd(uchar m)
{
	return maskChildren & m;
}

template<typename T>
inline bool OctreeNode<T>::maskObjectsAnd(uchar m)
{
	return maskObjects & m;
}

inline v3df boundPos(Boundf* b)
{
	return v3df{ b->x, b->y, b->z };
}

inline v3df boundSize(Boundf* b)
{
	return v3df{ b->w, b->h, b->d };
}

inline v4df bound4Size(Boundf* b)
{
	return v4df{ b->w, b->h, b->d, 0.0f };
}

template<typename T, std::uintmax_t max_depth>
Octree<T, max_depth>::Octree(v3df pos, v3df hsize)
{
	this->pos = pos;
	this->hsize = hsize;

	OCNode = new OctreeNode<T>();
	OCNode->setHMask(0xff);

	OCNode->children[0] = new OctreeNode<T>();
	OCNode->children[1] = new OctreeNode<T>();
	OCNode->children[2] = new OctreeNode<T>();
	OCNode->children[3] = new OctreeNode<T>();
	OCNode->children[4] = new OctreeNode<T>();
	OCNode->children[5] = new OctreeNode<T>();
	OCNode->children[6] = new OctreeNode<T>();
	OCNode->children[7] = new OctreeNode<T>();

	//OCObject = new Node<T>();

	OCTD = new OctreeTempData<T>{};
}

template<typename T, std::uintmax_t max_depth>
Octree<T, max_depth>::Octree()
{
}

template<typename T, std::uintmax_t max_depth>
inline bool __fastcall Octree<T, max_depth>::collideBoundfp(Boundf* tbound, Boundf* bound)
{
	return (bound->x <= tbound->x + tbound->w && bound->x + bound->w >= tbound->x) &&
		(bound->y <= tbound->y + tbound->h && bound->y + bound->h >= tbound->y) &&
		(bound->z <= tbound->z + tbound->d && bound->z + bound->d >= tbound->z);
}

template<typename T, std::uintmax_t max_depth>
inline bool Octree<T, max_depth>::vcollideBoundfp(Boundf* tbound, Boundf* bound)
{
	return false;
}

template<typename T, std::uintmax_t max_depth>
inline bool __fastcall Octree<T, max_depth>::collideBoundf(Boundf tbound, Boundf bound)
{
	return (bound.x <= tbound.x + tbound.w && bound.x + bound.w >= tbound.x) &&
		(bound.y <= tbound.y + tbound.h && bound.y + bound.h >= tbound.y) &&
		(bound.z <= tbound.z + tbound.d && bound.z + bound.d >= tbound.z);
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::insert(T obj, Boundf bound)
{
	dpth = 0;
	OCTD->obj = new OctreeObject<T>{ obj, bound };
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo B
	{																			//oo
		insertIns(OCNode->children[0]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox B
	{																			//oo
		insertIns(OCNode->children[1]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//ox
		insertIns(OCNode->children[2]);
		return;
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//xo
		insertIns(OCNode->children[3]);
		return;
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo F
	{																			//oo
		insertIns(OCNode->children[4]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox F
	{																			//oo
		insertIns(OCNode->children[5]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//ox
		insertIns(OCNode->children[6]);
		return;
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//xo
		insertIns(OCNode->children[7]);
		return;
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collide(Boundf bound)
{
	Boundf* bfp = &bound;
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, bfp))						//xo B
	{																			//oo
		collideIns(OCNode->children[0], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox B
	{																			//oo
		collideIns(OCNode->children[1], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//ox
		collideIns(OCNode->children[2], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//xo
		collideIns(OCNode->children[3], bfp, 1);
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//xo F
	{																			//oo
		collideIns(OCNode->children[4], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox F
	{																			//oo
		collideIns(OCNode->children[5], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//ox
		collideIns(OCNode->children[6], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//xo
		collideIns(OCNode->children[7], bfp, 1);
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::insertIns(OctreeNode<T>* node)
{
	dpth++;
	v3df pos = boundPos(&OCTD->bound);
	v3df hsize = boundSize(&OCTD->bound);
	hsize.x = hsize.x / 2.0f; hsize.y = hsize.y / 2.0f; hsize.z = hsize.z / 2.0f;
	//OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo B
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_BACK;
				node->children[0] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[0], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_BACK)) {
			insertIns(node->children[0]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
			{
				node->children[0] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_UPPER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_UPPER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[0];
				node->children[0] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[0]);
				OCTD->obj = tempObject;
				OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
				dpth = sdpth;
				insertIns(node->children[0]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox B
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				node->children[1] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[1], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_BACK)) {
			insertIns(node->children[1]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
			{
				node->children[1] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[1];
				node->children[1] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[1]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[1]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//ox
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_LOWER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_BACK;
				node->children[2] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[2], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_BACK)) {
			insertIns(node->children[2]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
			{
				node->children[2] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_LOWER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_LOWER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[2];
				node->children[2] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[2]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[2]);
				return;
			}
		}
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//xo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_LOWER_BACK;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_BACK;
				node->children[3] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[3], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_BACK)) {
			insertIns(node->children[3]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
			{
				node->children[3] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_LOWER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_LOWER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[3];
				node->children[3] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[3]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[3]);
				return;
			}
		}
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo F
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_UPPER_FRONT;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_FRONT;
				node->children[4] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[4], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_FRONT)) {
			insertIns(node->children[4]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT)) // mask 
			{
				node->children[4] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_UPPER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_UPPER_FRONT;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[4];
				node->children[4] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox F
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_FRONT;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_FRONT;
				node->children[5] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[5], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_FRONT)) {
			insertIns(node->children[5]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
			{
				node->children[5] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_UPPER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_UPPER_FRONT;
				tempObject = (OctreeObject<T>*)node->children[5];
				node->children[5] = new OctreeNode<T>();
				// save object pointer and call another insertIns for this object
				int sdpth = dpth;
				insertIns(node->children[5]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[5]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//ox
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_LOWER_FRONT;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_FRONT;
				node->children[6] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[6], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_FRONT)) {
			insertIns(node->children[6]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
			{
				node->children[6] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_LOWER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_LOWER_FRONT;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[6];
				node->children[6] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[6]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[6]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//xo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_LOWER_FRONT;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_FRONT;
				node->children[7] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[7], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_FRONT)) {
			insertIns(node->children[7]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
			{
				node->children[7] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_LOWER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_LOWER_FRONT;
				tempObject = (OctreeObject<T>*)node->children[7];
				node->children[7] = new OctreeNode<T>();
				// save object pointer and call another insertIns for this object
				int sdpth = dpth;
				insertIns(node->children[7]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[7]);
				return;
			}
		}
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collideIns(OctreeNode<T>* node, Boundf* bfp, int dpth_)
{
	v3df pos = boundPos(&OCTD->bound);
	v3df hsize = boundSize(&OCTD->bound);
	hsize.x = hsize.x / 2.0f; hsize.y = hsize.y / 2.0f, hsize.z = hsize.z / 2.0f;
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, bfp))						//xo B
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_BACK))
			collideIns(node->children[0], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[0];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[0])->bound, bfp))
						((OctreeObject<T>*)node->children[0])->object.render(); //TODO template function 

	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox B
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_BACK))
			collideIns(node->children[1], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[1];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[1])->bound, bfp))
						((OctreeObject<T>*)node->children[1])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_BACK))
			collideIns(node->children[2], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[2];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[2])->bound, bfp))
						((OctreeObject<T>*)node->children[2])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_BACK))
			collideIns(node->children[3], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[3];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[3])->bound, bfp))
						((OctreeObject<T>*)node->children[3])->object.render.render(); //TODO template function 
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//xo F
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_FRONT))
			collideIns(node->children[4], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[4];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[4])->bound, bfp))
						((OctreeObject<T>*)node->children[4])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox F
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_FRONT))
			collideIns(node->children[5], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[5];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[5])->bound, bfp))
						((OctreeObject<T>*)node->children[5])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_FRONT))
			collideIns(node->children[6], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[6];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[6])->bound, bfp))
						((OctreeObject<T>*)node->children[6])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_FRONT))
			collideIns(node->children[7], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[7];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[7])->bound, bfp))
						((OctreeObject<T>*)node->children[7])->object.render.render(); //TODO template function 
	}
}

//

void boundfRotate(Boundf* b, Boundf* t,  mat3 r)
{
	Boundf bd = Boundf{ b->x + b->w / 2, b->y + b->h / 2, b->z + b->d / 2 , b->w / 2, b->h / 2, b->d / 2};
	for (int i = 0; i < 3; i++)
	{
		((float*)t)[i] = 0.0f;
		((float*)t)[i + 3] = 0.0f;

		for (int l = 0; l < 3; l++)
		{
			((float*)t)[i] += r[i][l] * ((float*)&bd)[l];
			((float*)t)[i + 3] += abs(r[i][l]) * ((float*)&bd)[l + 3];
		}
	}
	*t = Boundf{t->x - t->w, t->y - t->h, t->z - t->d, t->w * 2, t->h * 2, t->d * 2};
}

#define DEBUG_SHADER
#ifdef DEBUG_SHADER
	#define DEBUG_BUFFER_SIZE 512
	char* debugOutput;
#endif

typedef unsigned int uint;
using namespace glm;
using namespace std::placeholders;

std::string* GetFileData(char* filename)
{
	std::ifstream stream(filename);
	std::string line;

	std::string* buffer = new std::string;
	while (std::getline(stream, line))
	{
		buffer->append(line);
		buffer->append("\n");
	}
	stream.close();
	return buffer;
}

class Camera
{
public:
	Camera(vec3 pos)
	{
		yaw = 90.0f; pitch = 0.0f; roll = 0.0f;
		this->pos = pos;

		f = normalize(vec3(
			cos(yaw) * cos(pitch),
			sin(pitch),
			cos(pitch) * sin(yaw)
		));
		r = normalize(cross(f, UP_VECTOR));
		u = normalize(cross(f, r));
	}
	mat4 getMatrix()
	{
		f = normalize(vec3(
			cos(yaw) * cos(pitch),
			sin(pitch),
			cos(pitch) * sin(yaw)
		));

		mat4 roll_mat = glm::rotate(mat4(1.0f), roll, f);

		r = normalize(cross(f, UP_VECTOR));
		u = mat3(roll_mat) * normalize(cross(f, r));
		mat4 M = lookAt(pos, f, u);
		return M;
	}
	vec3 getPos()
	{
		return pos;
	}
	vec3* getPPos()
	{
		return &pos;
	}
	void addYaw(float x)
	{
		yaw += x;
	}
	void addPitch(float x)
	{
		pitch += x;
	}
	void addRoll(float x)
	{
		roll += x;
	}
	void setMatrix(mat4* matrix)
	{
		f = normalize(vec3(
			cos(yaw) * cos(pitch),
			sin(pitch),
			cos(pitch) * sin(yaw)
		));

		mat4 roll_mat = glm::rotate(mat4(1.0f), roll, f);

		r = normalize(cross(f, UP_VECTOR));
		u = mat3(roll_mat) * normalize(cross(f, r));

		*matrix = lookAt(pos, f + pos, u);
	}
	void setYaw(float yaw)
	{
		this->yaw = yaw;
	}
	void setPitch(float pitch)
	{
		this->pitch = pitch;
	}
	void setRoll(float roll)
	{
		this->roll = roll;
	}
	vec3 getRight()
	{
		return r;
	}
	void setPos(vec3 pos)
	{
		this->pos = pos;
	}
	void addPos(vec3 pos)
	{
		this->pos += pos;
	}
	void posAddFront(float s)
	{
		pos += f * s;
	}
	void posAddRight(float s)
	{
		pos += r * s;
	}

	float yaw, pitch, roll;
	vec3 pos;
	vec3 r, u, f;
};

bool planeAABBIntersection(Boundf* b, Plane* p)
{
	Boundf bc = Boundf{ b->x + b->w / 2, b->y + b->h / 2, b->z + b->d / 2 , b->w / 2, b->h / 2, b->d / 2 };

	return true;
}

void planeNormalize(Plane* p)
{
	float l = sqrt(pow(p->p.x, 2) + pow(p->p.y, 2) + pow(p->p.z, 2));
	p->p.x = p->p.x / l;
	p->p.y = p->p.y / l;
	p->p.z = p->p.z / l;
	p->p.w = p->p.w / l;
}

void frustumNormalize(Frustum* f)
{
	planeNormalize(&f->near);
	planeNormalize(&f->far);
	planeNormalize(&f->right);
	planeNormalize(&f->up);
	planeNormalize(&f->down);
	planeNormalize(&f->up);
}

Frustum createFrustumFromCamera(mat4 vp)
{
	Frustum f;
	
	f.left.p.x = vp[0].w + vp[0].x;
	f.left.p.y = vp[1].w + vp[1].x;
	f.left.p.z = vp[2].w + vp[2].x;
	f.left.p.w = vp[3].w + vp[3].x;
	
	f.right.p.x = vp[0].w - vp[0].x;
	f.right.p.y = vp[1].w - vp[1].x;
	f.right.p.z = vp[2].w - vp[2].x;
	f.right.p.w = vp[3].w - vp[3].x;

	f.up.p.x = vp[0].w - vp[0].y;
	f.up.p.y = vp[1].w - vp[1].y;
	f.up.p.z = vp[2].w - vp[2].y;
	f.up.p.w = vp[3].w - vp[3].y;

	f.down.p.x = vp[0].w + vp[0].y;
	f.down.p.y = vp[1].w + vp[1].y;
	f.down.p.z = vp[2].w + vp[2].y;
	f.down.p.w = vp[3].w + vp[3].y;

	f.near.p.x = vp[0].z;
	f.near.p.y = vp[1].z;
	f.near.p.z = vp[2].z;
	f.near.p.w = vp[3].z;

	f.far.p.x = vp[0].w - vp[0].z;
	f.far.p.y = vp[1].w - vp[1].z;
	f.far.p.z = vp[2].w - vp[2].z;
	f.far.p.w = vp[3].w - vp[3].z;

	return f;

}

bool planeAABBIsOnForward(Plane* p, Boundf* b)
{
	vec3 a = vec3(0.0f);
	if (p->p.x < 0.0f)
		a.x = b->x;
	else
		a.x = b->x + b->w;

	if (p->p.y < 0.0f)
		a.y = b->y;
	else
		a.y = b->y + b->h;

	if (p->p.z < 0.0f)
		a.z = b->z;
	else
		a.z = b->z + b->d;

	float f = dot(vec3(p->p.x, p->p.y, p->p.z), a) + p->p.w;
	//float f = distance(vec3(p->p.x, p->p.y, p->p.z), a) + p->p.w;
	return f > 0.0f;
}

inline bool collideBoundfFrustumf(Boundf* tbound, Frustum* f)
{
	return (planeAABBIsOnForward(&f->near, tbound) &&
		planeAABBIsOnForward(&f->far, tbound) &&
		planeAABBIsOnForward(&f->left, tbound) &&
		planeAABBIsOnForward(&f->right, tbound) &&
		planeAABBIsOnForward(&f->up, tbound) &&
		planeAABBIsOnForward(&f->down, tbound));
}

template<typename T, std::uintmax_t max_depth>
inline bool Octree<T, max_depth>::collideBoundfFrustum(Boundf* tbound, Frustum* f) // changed to ||
{
	return (planeAABBIsOnForward(&f->near, tbound) &&
		planeAABBIsOnForward(&f->far, tbound) &&
		planeAABBIsOnForward(&f->left, tbound) &&
		planeAABBIsOnForward(&f->right, tbound) &&
		planeAABBIsOnForward(&f->up, tbound) &&
		planeAABBIsOnForward(&f->down, tbound));
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collideFrustum(Frustum* bfp)
{
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//xo B
	{																			//oo
		collideInsFrustum(OCNode->children[0], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//ox B
	{																			//oo
		collideInsFrustum(OCNode->children[1], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo B
	{																			//ox
		collideInsFrustum(OCNode->children[2], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo B
	{																			//xo
		collideInsFrustum(OCNode->children[3], bfp, 1);
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//xo F
	{																			//oo
		collideInsFrustum(OCNode->children[4], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//ox F
	{																			//oo
		collideInsFrustum(OCNode->children[5], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo F
	{																			//ox
		collideInsFrustum(OCNode->children[6], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo F
	{																			//xo
		collideInsFrustum(OCNode->children[7], bfp, 1);
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collideInsFrustum(OctreeNode<T>* node, Frustum* bfp, int dpth_)
{
	v3df pos = boundPos(&OCTD->bound);
	v3df hsize = boundSize(&OCTD->bound);
	hsize.x = hsize.x / 2.0f; hsize.y = hsize.y / 2.0f, hsize.z = hsize.z / 2.0f;
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//xo B
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_BACK))
			collideInsFrustum(node->children[0], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[0];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[0])->bound, bfp))
						((OctreeObject<T>*)node->children[0])->object.render(); //TODO template function 

	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//ox B
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_BACK))
			collideInsFrustum(node->children[1], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[1];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[1])->bound, bfp))
						((OctreeObject<T>*)node->children[1])->object.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo B
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_BACK))
			collideInsFrustum(node->children[2], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[2];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[2])->bound, bfp))
						((OctreeObject<T>*)node->children[2])->object.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo B
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_BACK))
			collideInsFrustum(node->children[3], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[3];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[3])->bound, bfp))
						((OctreeObject<T>*)node->children[3])->object.render(); //TODO template function 
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//xo F
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_FRONT))
			collideInsFrustum(node->children[4], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[4];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[4])->bound, bfp))
						((OctreeObject<T>*)node->children[4])->object.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//ox F
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_FRONT))
			collideInsFrustum(node->children[5], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[5];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[5])->bound, bfp))
						((OctreeObject<T>*)node->children[5])->object.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo F
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_FRONT))
			collideInsFrustum(node->children[6], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[6];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[6])->bound, bfp))
						((OctreeObject<T>*)node->children[6])->object.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfFrustum(&OCTD->bound, bfp))						//oo F
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_FRONT))
			collideInsFrustum(node->children[7], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[7];
					do
					{
						if (collideBoundfFrustum(&N->data->bound, bfp))
							N->data->object.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfFrustum(&((OctreeObject<T>*)node->children[7])->bound, bfp))
						((OctreeObject<T>*)node->children[7])->object.render(); //TODO template function 

	}
}

void bindUBORenderer(void* m, void* l, UniformBufferObjectSelector* sc)
{
	sc->ubo->bind();
	sc->ubo->subData(sc->offset, sc->size, m);
}

class OctreeRenderModelIndex
{
public:
	Boundf* bound;
	bool* render_;

	OctreeRenderModelIndex(bool* render, Boundf* bound)
	{
		this->bound = bound;
		this->render_ = render;
	}

	void render()
	{
		*this->render_ = true;
	}
};

template<typename T>
T lerp(T min, T max, float v)
{
	return min + (max - min) * v;
}

#define WINSIZE_X 1920
#define WINSIZE_Y 1080

int main()
{
	SDL_Init(SDL_INIT_VIDEO);
	SDL_Window* win = SDL_CreateWindow("fiutex", 0, 0,
		WINSIZE_X, WINSIZE_Y, SDL_WINDOW_OPENGL);
	SDL_Event e;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

	SDL_GL_CreateContext(win);
	glewInit();

#ifdef DEBUG_SHADER
	debugOutput = new char[DEBUG_BUFFER_SIZE];
#endif

	const char* vertexSource = GetFileData((char*)"vertex.glsl")->c_str();//readfile("vertex.glsl");
	const char* fragmentSource = GetFileData((char*)"frag.glsl")->c_str();//readfile("frag.glsl");
	const char* fragmentNoTexSource = GetFileData((char*)"fragNoTex.glsl")->c_str();
	const char* tessEvalSource = GetFileData((char*)"tessEval.glsl")->c_str();
	const char* tessContrSource = GetFileData((char*)"tesControll.glsl")->c_str();
	const char* fragmentTerrainSource = GetFileData((char*)"fragmentTerrain.glsl")->c_str();
	const char* tessVertexSource = GetFileData((char*)"TessVertex.glsl")->c_str();
	const char* computeHeightMapGen = GetFileData((char*)"HeightMapGenerator.glsl")->c_str();
	const char* computeNormalMapGen = GetFileData((char*)"NormalGeneratorHMap.glsl")->c_str();

	uint shaderFragmentNoTex = compileShader(GL_FRAGMENT_SHADER, fragmentNoTexSource);
	uint shaderTessVertex = compileShader(GL_VERTEX_SHADER, tessVertexSource);
	uint shaderComputeHeightMap = compileShader(GL_COMPUTE_SHADER, computeHeightMapGen);
	uint shaderComputeNormalMap = compileShader(GL_COMPUTE_SHADER, computeNormalMapGen);

	uint heightMapGeneratorProgram = glCreateProgram();
	glAttachShader(heightMapGeneratorProgram, shaderComputeHeightMap);
	glLinkProgram(heightMapGeneratorProgram);

	uint normalMapGeneratorProgram = glCreateProgram();
	glAttachShader(normalMapGeneratorProgram, shaderComputeNormalMap);
	glLinkProgram(normalMapGeneratorProgram);

	uint shaderVertex = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(shaderVertex, 1, &vertexSource, NULL);
	glCompileShader(shaderVertex);

	uint shaderFragment = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(shaderFragment, 1, &fragmentSource, NULL);
	glCompileShader(shaderFragment);

	uint programNoTex = compileProgram(shaderVertex, shaderFragmentNoTex);

	uint shaderTessEval = compileShader(GL_TESS_EVALUATION_SHADER, tessEvalSource);
	uint shaderTessContr = compileShader(GL_TESS_CONTROL_SHADER, tessContrSource);
	uint shaderTerrainFragment = compileShader(GL_FRAGMENT_SHADER, fragmentTerrainSource);
	uint terrainProgramPatch16 = glCreateProgram();
	glAttachShader(terrainProgramPatch16, shaderTessVertex);
	glAttachShader(terrainProgramPatch16, shaderTessContr);
	glAttachShader(terrainProgramPatch16, shaderTessEval);
	glAttachShader(terrainProgramPatch16, shaderTerrainFragment);
	glLinkProgram(terrainProgramPatch16);

#ifdef DEBUG_SHADER
	int vertexCode, fragmentCode;
	GLsizei length;
	glGetShaderiv(shaderTessContr, GL_COMPILE_STATUS, &vertexCode);
	glGetShaderiv(shaderTessVertex, GL_COMPILE_STATUS, &fragmentCode);
	if (vertexCode == 0)
	{
		glGetShaderInfoLog(shaderComputeNormalMap, DEBUG_BUFFER_SIZE, &length, debugOutput);
		printf(debugOutput);
	}
	if (fragmentCode == 0)
	{
		glGetShaderInfoLog(shaderFragment, GL_COMPILE_STATUS, &length, debugOutput);
		printf(debugOutput);
	}
	if (fragmentCode == 0 or vertexCode == 0)
		exit(2);
#endif

	uint program = glCreateProgram();
	glAttachShader(program, shaderVertex);
	glAttachShader(program, shaderFragment);
	glLinkProgram(program);

	glDeleteShader(shaderVertex);
	glDeleteShader(shaderFragment);
	glDeleteShader(shaderVertex);
	glDeleteShader(shaderFragment);
	glDeleteShader(shaderFragmentNoTex);

	mat4 bezierBasisMatrix = mat4(1.0f);
	bezierBasisMatrix[0] = vec4(1.0f, -3.0f, 3.0f, -1.0f);
	bezierBasisMatrix[1] = vec4(0.0f, 3.0f, -6.0f, 3.0f);
	bezierBasisMatrix[2] = vec4(0.0f, 0.0f, 3.0f, -3.0f);
	bezierBasisMatrix[3] = vec4(0.0f, 0.0f, 0.0f, 1.0f);
	bezierBasisMatrix = transpose(bezierBasisMatrix);
	float terrainTessellationLevel = 5.0f;

	glUseProgram(terrainProgramPatch16);
	uint basisL, tessLevelL;
	basisL = glGetUniformLocation(terrainProgramPatch16, "basis");
	tessLevelL = glGetUniformLocation(terrainProgramPatch16, "TessLevel");

	glUniformMatrix4fv(basisL, 1, GL_FALSE, &bezierBasisMatrix[0].x);
	glUniform1f(tessLevelL, terrainTessellationLevel);


	vec3 lightPos = vec3(10.0f, 1.5f, 10.0f);
	vec4 lightColor = vec4(1.0f, 1.0f, 1.0f, 1.0f);

	mat4 projectionMatrix = perspective(radians(70.0f), (float)(WINSIZE_X / WINSIZE_Y), 0.01f, 160.0f);
	mat4 modelMatrix = mat4(1.0f);
	mat4 cameraMatrix = lookAt(
		vec3(0.0f, 0.0f, 1.5f),
		vec3(0.0f, 0.0f, 0.0f),
		vec3(0.0f, 1.0f, 0.0f)
	);

	float zNearPlane = 0.01f, zFarPlane = 150.0f, zFov = 90.0f, zAspect = (float)(WINSIZE_X / WINSIZE_Y);
	modelMatrix = translate(modelMatrix, vec3(0.0f, 0.0f, -0.5f));
	glUseProgram(program);
	ivec2 tempIvec2;
	vec4 tempVec4;

	//patches
	/*
	glUseProgram(heightMapGeneratorProgram);
	Texture2D hMap = Texture2D(GL_R32F, GL_REPEAT, 500, 500);
	Texture2D nMap = Texture2D(GL_RGBA32F, GL_REPEAT, 500, 500);
	hMap.bindImage(0, GL_WRITE_ONLY);

	UniformBuffer imgStat = UniformBuffer(32, GL_STREAM_DRAW, 0);
	tempIvec2 = ivec2(hMap.x, hMap.y);
	tempVec4 = vec4(21.5f, 0.6f, 4.5f, 0.0f);
	imgStat.subData(0, 8, &tempIvec2);
	imgStat.subData(8, 12, &tempVec4);
	imgStat.bindBaseTo(1);

	glDispatchCompute(hMap.x, hMap.y, 1);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);
	*/
	Texture2D hMap = Texture2D("map2.png", GL_R32F, GL_REPEAT); // hm.png - test
	Texture2D nMap = Texture2D(GL_RGBA32F, GL_REPEAT, hMap.x, hMap.y);
	//hMap.bindImage(0, GL_WRITE_ONLY);
	//UniformBuffer imgStat = UniformBuffer(32, GL_STREAM_DRAW, 0);
	//tempIvec2 = ivec2(hMap.x, hMap.y);x
	//imgStat.subData(0, 8, &tempIvec2);

	
	nMap.bindImage(1, GL_WRITE_ONLY);
	hMap.bindImage(0, GL_READ_WRITE); // GL_READ_ONLY - without height adjustment
	glUseProgram(normalMapGeneratorProgram);
	//imgStat.bindBaseTo(2);
	glDispatchCompute(nMap.x, nMap.y, 1);
	glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

	nMap.bind();
	nMap.mipmapgen();
	hMap.bind();
	hMap.mipmapgen();

	int patchsize;
	float* patches = generateHeightMapRects(2.0f, 250.0f, 250.0f, &patchsize);//generatePatchArray(1.0f, 128.0f, 128.0f, &patchsize);
	//{
	//	0.0f, -3.0f, 0.0f,    1.0f, 0.0f, 0.0f,     2.0f, 0.0f, 0.0f,      3.0f, 1.0f, 0.0f,
	//	0.0f, 0.0f, 1.0f,     1.0f, 0.0f, 1.0f,     2.0f, 0.0f, 1.0f,      3.0f, 0.0f, 1.0f,
	//	0.0f, 0.0f, 2.0f,     1.0f, 0.0f, 2.0f,     2.0f, 0.0f, 2.0f,      3.0f, 0.0f, 2.0f,
	//	0.0f, 0.0f, 3.0f,     1.0f, 0.0f, 3.0f,     2.0f, 0.0f, 3.0f,      3.0f, -0.5f, 3.0f,
	//};

	vBuffer terrainVAO = vBuffer();
	Buffer terrainVbo = Buffer(patches, patchsize * 4, GL_ARRAY_BUFFER, GL_STATIC_DRAW);
	terrainVAO.addAttrib(2, GL_FLOAT, 8, 0);
	terrainVAO.enableAttrib();

	//patches
	glEnable(GL_DEPTH_TEST);
#ifdef FACE_CULL_ENABLE
	glFrontFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
#endif
	std::vector<float>* vArr = new std::vector<float>();

	Camera cam(vec3(0.0f, 0.0f, -2.0f));

	Buffer tBuffer = Buffer();
	Renderer rr(-1);
	loadObjToList(vArr, (char*)"mtl.obj", 1024 * 1024);
	vBuffer* loadedVao = vertexStoreArray(vArr, &tBuffer, STORE_ATTRIB_VERTEX_TEXTURE_NORMAL);
	rr.addTarget(2, new RenderTarget(loadedVao, nullptr, vArr->size(), 2, TARGET_RENDER_ARRAY));
	freeObjMemoryFill(vArr);
	loadObjToList(vArr, (char*)"cube.obj", 288 * 4); // cube.obj 288 * 4
	loadedVao = vertexStoreArray(vArr, &tBuffer, STORE_ATTRIB_VERTEX_TEXTURE_NORMAL);
	rr.addTarget(4, new RenderTarget(loadedVao, nullptr, vArr->size(), 5000, TARGET_RENDER_ARRAY));
	freeObjMemoryFill(vArr);
	loadObjToList(vArr, (char*)"tree2.obj", 1024 * 8);
	loadedVao = vertexStoreArray(vArr, &tBuffer, STORE_ATTRIB_VERTEX_TEXTURE_NORMAL);
	rr.addTarget(3, new RenderTarget(loadedVao, nullptr, vArr->size(), 5000, TARGET_RENDER_ARRAY));
	freeObjMemory(vArr);

	

	// - end
	UniformBuffer tMatrixUbo = UniformBuffer(sizeof(mat4) * 3, GL_STREAM_DRAW, 0);
	UniformBuffer lightUbo = UniformBuffer(32, GL_DYNAMIC_DRAW, 1);
	UniformBuffer cameraValuesUbo = UniformBuffer(80, GL_STREAM_DRAW, 2);
	mat4 sendMatrixPV = projectionMatrix * cameraMatrix;

	tMatrixUbo.bind();
	tMatrixUbo.subData(0, SIZEOF_MAT4, &modelMatrix[0].x);
	tMatrixUbo.subData(SIZEOF_MAT4, SIZEOF_MAT4, &sendMatrixPV[0].x);
	tMatrixUbo.subData(SIZEOF_MAT4 * 2, SIZEOF_MAT4, nullptr);
	tMatrixUbo.bindBase();

	lightUbo.bind();
	lightUbo.subData(0, SIZEOF_VEC3, &lightPos.x);
	lightUbo.subData(SIZEOF_VEC4, SIZEOF_VEC4, &lightColor.x);
	lightUbo.bindBase();

	cameraValuesUbo.bind();
	cameraValuesUbo.subData(0, 12, cam.getPPos());
	cameraValuesUbo.subData(16, SIZEOF_MAT4, &mat4(1.0f)[0].x);
	cameraValuesUbo.bindBase();

	UniformBufferObjectSelector modelSec{ &tMatrixUbo, 0, 64};
	auto fc = std::bind(&bindUBORenderer, _1, _2, &modelSec);

	glClearColor(0.5f, 0.0f, 0.0f, 1.0f);
	float r = 0.0f;
	glUseProgram(terrainProgramPatch16);
	glUniform1i(glGetUniformLocation(terrainProgramPatch16, "terrain"), 2);
	glUniform1i(glGetUniformLocation(terrainProgramPatch16, "hMap"), 0);
	glUniform1i(glGetUniformLocation(terrainProgramPatch16, "nMap"), 1);
	glViewport(0, 0, WINSIZE_X, WINSIZE_Y);
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		// FRUSTUM

	cam.setMatrix(&cameraMatrix);
	Octree<OctreeRenderModelIndex, 32> frustumCullingTree = Octree<OctreeRenderModelIndex, 32>(v3df{ 0.0f, 0.0f, 0.0f }, v3df{ 500.0f, 500.0f, 500.0f });
	Frustum mainFrustum = createFrustumFromCamera(cameraMatrix * projectionMatrix);
	frustumNormalize(&mainFrustum);

	int trees = 1000;
	Boundf cubeBound;
	vec3 treePos;
	int xm, ym;
	for (int i = 1; i < trees+1; i++)
	{
		treePos = vec3(rand() % 5000 / 10.0, 0.0f, rand() % 5000 / 10);
		//treePos = vec3(0.0f, 0.0f, 40.0f);
		xm = lerp(0, hMap.x, treePos.x / 500.0);
		ym = lerp(0, hMap.y, treePos.z / 500.0);
		treePos.y = (unsigned char)(*hMap.sample(xm, ym));
		treePos.y = ((treePos.y) / 255.0f) * 50.0f;
		treePos += vec3(-1.0f, 0.0f, -1.0f);
		rr.getTarget(3)->addNewObject(); // 3 - cube.obj

		cubeBound = Boundf{ treePos.x, treePos.y, treePos.z, 2.0f, 2.0f, 2.0f }; // bound for cube

		rr.targetSetMatrix(3, i, rotate(translate(mat4(1.0f), treePos), radians(180.0f), vec3(1,0,0)));

		frustumCullingTree.insert(OctreeRenderModelIndex(rr.getTarget(3)->getRenderLocation(i), &cubeBound), cubeBound);
	}

	//

	int polymode = 0;
	Texture2D grassTexture = Texture2D("grs.png", GL_RGB, GL_REPEAT);
	mat4 scaleMatrixWorld = mat4(1.0f);
	mat4 scaleMatrixTree = scale(mat4(1.0f), vec3(0.5f));
	
	while (true)
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		cam.setMatrix(&cameraMatrix);

		glUseProgram(programNoTex);

		//modelMatrix = translate(mat4(1.0f), vec3(-0.5f, -0.5f, 0.0f));
		//modelMatrix = rotate(modelMatrix, (float)M_PI, vec3(1, 0, 0));
		tMatrixUbo.bind();
		sendMatrixPV = projectionMatrix * cameraMatrix;
		tMatrixUbo.subData(SIZEOF_MAT4, SIZEOF_MAT4, &sendMatrixPV[0].x);
		tMatrixUbo.subData(SIZEOF_MAT4 * 2, SIZEOF_MAT4, &scaleMatrixTree);

		rr.targetSetMatrix(3,0,modelMatrix);
		rr.renderCustomBind(3, fc);

		glUseProgram(terrainProgramPatch16);
		
		texUnitActivate(GL_TEXTURE2);
		grassTexture.bind();
		texUnitActivate(GL_TEXTURE0);
		hMap.bind();
		texUnitActivate(GL_TEXTURE1);
		nMap.bind();
		glPatchParameteri(GL_PATCH_VERTICES, 4);
		terrainVAO.bind();
		
		modelMatrix = mat4(1.0);
		tMatrixUbo.subData(0, 64, &modelMatrix);
		tMatrixUbo.subData(SIZEOF_MAT4 * 2, SIZEOF_MAT4, &scaleMatrixWorld);

		glDrawArrays(GL_PATCHES, 0, patchsize / 3.0f);

		SDL_GL_SwapWindow(win);
		SDL_Delay(25); 
		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				exit(1);
			}
			
			else if (e.type == SDL_KEYDOWN)
			{
				if (e.key.keysym.sym == SDLK_s)
					cam.addPitch(0.1f);
				if (e.key.keysym.sym == SDLK_w)
					cam.addPitch(-0.1f);
				if (e.key.keysym.sym == SDLK_a)
					cam.addYaw(0.1f);
				if (e.key.keysym.sym == SDLK_d)
					cam.addYaw(-0.1f);
				if (e.key.keysym.sym == SDLK_LEFT)
					cam.posAddRight(0.80f);
				if (e.key.keysym.sym == SDLK_RIGHT)
					cam.posAddRight(-0.80f);
				if (e.key.keysym.sym == SDLK_UP)
					cam.posAddFront(0.80f);
				if (e.key.keysym.sym == SDLK_DOWN)
					cam.posAddFront(-0.80f);
				if (e.key.keysym.sym == SDLK_q)
					cam.addRoll(-0.1f);
				if (e.key.keysym.sym == SDLK_e)
					cam.addRoll(0.1f);
				if (e.key.keysym.sym == SDLK_z)
				{
					if (polymode == 0)
					{
						polymode = 1;
						glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
					}
					else
					{
						polymode = 0;
						glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
					}
					continue;
				}
				lightUbo.bind();
				lightUbo.subData(0, 12, cam.getPPos());
				cameraValuesUbo.bind();
				cameraValuesUbo.subData(0, 12, cam.getPPos());
				cameraValuesUbo.subData(16, SIZEOF_MAT4, &cameraMatrix[0].x);

				mainFrustum = createFrustumFromCamera(projectionMatrix * cameraMatrix);
				frustumNormalize(&mainFrustum);
				rr.renderClearTragetAll(0, 3);
				rr.renderClearTargetSelect(1, 3, 0);
				frustumCullingTree.collideFrustum(&mainFrustum);
			}
			
		}
	}
}