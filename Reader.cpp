#include "Reader.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

char* read(const char* path, int* x, int* y, int* mode)
{
	return (char*)stbi_load(path, x, y, mode, 0);
}