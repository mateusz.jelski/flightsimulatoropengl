#include "TerrainGen.h"

int pseed = 12345;

float* generatePatchArray(float patchSize, float patchX, float patchY, int* s)
{
    float* patchArray = new float[3 * 16 * patchX * patchY];
    *s = 3 * 16 * patchX * patchY;
    int arrayIter = 0;
    auto perlin = noise::module::Perlin();
    auto perlinLF = noise::module::Perlin();

    float padd = patchSize / 3;

    perlinLF.SetFrequency(0.06);
    perlinLF.SetOctaveCount(2);
    perlinLF.SetLacunarity(8.0);
    perlinLF.SetPersistence(0.025);
    
    perlin.SetFrequency(0.25);
    perlin.SetOctaveCount(4);
    perlin.SetLacunarity(1.2);
    perlin.SetPersistence(0.05);

    for (float x = 0.0f; x < patchX * patchSize; x += patchSize)
    {
        for (float y = 0.0f; y < patchY * patchSize; y += patchSize)
        {
            
            for (int ppx = 0; ppx < 4; ppx++)
            {
                for (int ppy = 0; ppy < 4; ppy++)
                {
                    patchArray[arrayIter    ] = x + ppx * padd;
                    patchArray[arrayIter + 1] = (7.0 * (float)perlinLF.GetValue(x + ppx * padd, y + ppy * padd, 0.0)) + (float)perlin.GetValue(x + ppx * padd, y + ppy * padd, 0.0);
                    patchArray[arrayIter + 2] = y + ppy * padd;
                    arrayIter+=3;
                }
            }

        }
    }
    return patchArray;
}

float* generateHeightMapRects(float rs, float amountx, float amounty, int* s)
{
    float* arr = new float[4 * 2 * amountx * amounty];
    int arri = 0;
    *s = 4 * 2 * amountx * amounty;

    for (float x = 0.0f; x < amountx * rs; x += rs)
    {
        for (float y = 0.0f; y < amounty * rs; y += rs)
        {
            arr[arri] = x;
            //arr[arri + 1] = 0.0f;
            arr[arri + 1] = y;

            arr[arri + 2] = x + rs;
            //arr[arri + 4] = 0.0f;
            arr[arri + 3] = y;

            arr[arri + 4] = x;
            //arr[arri + 7] = 0.0f;
            arr[arri + 5] = y + rs;

            arr[arri + 6] = x + rs;
            //arr[arri + 10] = 0.0f;
            arr[arri + 7] = y + rs;

            arri += 8;//12;


        }
    }

    return arr;
}
