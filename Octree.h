#pragma once
#include <list>
#include <vector>
#include <iostream>

#include <ctime>

struct v3df
{
	float x, y, z;
};

struct v4df
{
	float x, y, z, w;
};

typedef unsigned char uchar;

template<typename T>
class NodeWrapper
{
public:
	T obj;
};

struct Boundf
{
	float x, y, z, h, w, d;
};

#define prefetch(S) _mm_prefetch((const char*)S, _MM_HINT_T2)

template<typename T>
struct OctreeObject
{
	T object;
	Boundf bound;
};

template<typename T>
struct OctreeLNode
{
	T data;
	OctreeLNode<T>* next;
};

template<typename T>
void appendFront(OctreeLNode<T>** node, T data);

template<typename T>
class OctreeListRef
{
public:
	short amount;
	OctreeLNode<T>* list;
};

template<typename T>
class OctreeNode
{
public:
	OctreeNode();

	//bool canInsert() { return objectPointer == nullptr; }
	bool maskChildrenAnd(uchar m); //{ return maskChildren & m; }
	bool maskObjectsAnd(uchar m); //{ return maskObjects & m; }
	void setHMask(uchar mask); //{ maskChildren = mask; }
	void setOMask(uchar mask); //{ maskObjects = mask; }


	OctreeNode* children[8];
	uchar maskChildren;
	uchar maskObjects;
};

#define MASK_RIGHT_UPPER_FRONT (uchar)1
#define MASK_RIGHT_LOWER_FRONT (uchar)2
#define MASK_LEFT_UPPER_FRONT  (uchar)4
#define MASK_LEFT_LOWER_FRONT  (uchar)8
#define MASK_RIGHT_UPPER_BACK  (uchar)16
#define MASK_RIGHT_LOWER_BACK  (uchar)32
#define MASK_LEFT_UPPER_BACK   (uchar)64
#define MASK_LEFT_LOWER_BACK   (uchar)128

v3df boundPos(Boundf* b);
v3df boundSize(Boundf* b);

typedef int halfSizei;

template<typename T>
struct OctreeTempData
{
	Boundf bound;
	OctreeObject<T>* obj;
};

template<typename T, std::uintmax_t max_depth>
class Octree
{
public:
	Octree(v3df pos, v3df hsize);
	Octree();
	v3df pos; v3df hsize;
	OctreeTempData<T>* OCTD;
	OctreeObject<T>* tempObject;
	int dpth;

	inline bool __fastcall collideBoundf(Boundf tbound, Boundf bound);
	inline bool __fastcall collideBoundfp(Boundf* tbound, Boundf* bound);
	inline bool __fastcall vcollideBoundfp(Boundf* tbound, Boundf* bound);
	void insert(T obj, Boundf bound);
	void collide(Boundf bound);


private:

	void insertIns(OctreeNode<T>* node);
	void collideIns(OctreeNode<T>* node, Boundf* bfp, int dpth);

	//Node<OctreeObject<T>>* OCObject;
	OctreeNode<T>* OCNode;
};