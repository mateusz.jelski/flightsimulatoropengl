#version 420 core
#extension GL_ARB_tessellation_shader : enable

uniform mat4 basis;

layout (quads, equal_spacing, ccw) in;

patch in vec2 c_oUv;
patch in vec3 rPos;

layout (std140, binding = 0) uniform MUBO
{
 mat4 model;
 mat4 PV;
} mubo;

layout (std140, binding = 2) uniform Camera
{
	vec3 cameraPosition;
	mat4 cameraMatrix;
} cam;

uniform sampler2D nMap;
uniform sampler2D hMap;

out vec2 toUv;
out vec4 tcpos;
out vec3 tnormal;


void main()
{
/*
vec3 p00 = gl_in[ 0 ].gl_Position.xyz;
vec3 p10 = gl_in[ 1 ].gl_Position.xyz;
vec3 p20 = gl_in[ 2 ].gl_Position.xyz;
vec3 p30 = gl_in[ 3 ].gl_Position.xyz;
vec3 p01 = gl_in[ 4 ].gl_Position.xyz;
vec3 p11 = gl_in[ 5 ].gl_Position.xyz;
vec3 p21 = gl_in[ 6 ].gl_Position.xyz;
vec3 p31 = gl_in[ 7 ].gl_Position.xyz;
vec3 p02 = gl_in[ 8 ].gl_Position.xyz;
vec3 p12 = gl_in[ 9 ].gl_Position.xyz;
vec3 p22 = gl_in[ 10 ].gl_Position.xyz;
vec3 p32 = gl_in[ 11 ].gl_Position.xyz;
vec3 p03 = gl_in[ 12 ].gl_Position.xyz;
vec3 p13 = gl_in[ 13 ].gl_Position.xyz;
vec3 p23 = gl_in[ 14 ].gl_Position.xyz;
vec3 p33 = gl_in[ 15 ].gl_Position.xyz;

	toUv = c_oUv;
	//tnormal = c_normal;
	//tcpos = c_cpos;

	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;

	vec4 vpos;
	vec4 tp;

	vec3 positionDU, positionDV;

	// for x coord
	mat4 pointsCoordMat;// = mat4(1.0);
	pointsCoordMat[0] = vec4(p00.x, p01.x, p02.x, p03.x);
	pointsCoordMat[1] = vec4(p10.x, p11.x, p12.x, p13.x);
	pointsCoordMat[2] = vec4(p20.x, p21.x, p22.x, p23.x);
	pointsCoordMat[3] = vec4(p30.x, p31.x, p32.x, p33.x);

	mat4 tb = transpose(basis);

	vec4 uvec = vec4(1.0, u, u * u, u * u * u);
	vec4 vvec = vec4(1.0, v, v * v, v * v * v);

	vec4 duvec = vec4(0.0, 1.0, 2 * u, 3 * u * u);
	vec4 dvvec = vec4(0.0, 1.0, 2 * v, 3 * v * v);

	tp = vvec * tb * pointsCoordMat * basis * uvec;
	vpos.x = tp.x + tp.y + tp.z + tp.w;

	tp = dvvec * tb * pointsCoordMat * basis * uvec;
	positionDV.x = tp.x + tp.y + tp.z + tp.w;
	tp = vvec * tb * pointsCoordMat * basis * duvec;
	positionDU.x = tp.x + tp.y + tp.z + tp.w;

	// for y coord
	pointsCoordMat[0] = vec4(p00.y, p01.y, p02.y, p03.y);
	pointsCoordMat[1] = vec4(p10.y, p11.y, p12.y, p13.y);
	pointsCoordMat[2] = vec4(p20.y, p21.y, p22.y, p23.y);
	pointsCoordMat[3] = vec4(p30.y, p31.y, p32.y, p33.y);

	tp = vvec * tb * pointsCoordMat * basis * uvec;
	vpos.y = tp.x + tp.y + tp.z + tp.w;
	tp = dvvec * tb * pointsCoordMat * basis * uvec;
	positionDV.y = tp.x + tp.y + tp.z + tp.w;
	tp = vvec * tb * pointsCoordMat * basis * duvec;
	positionDU.y = tp.x + tp.y + tp.z + tp.w;

	// for z coord
	pointsCoordMat[0] = vec4(p00.z, p01.z, p02.z, p03.z);
	pointsCoordMat[1] = vec4(p10.z, p11.z, p12.z, p13.z);
	pointsCoordMat[2] = vec4(p20.z, p21.z, p22.z, p23.z);
	pointsCoordMat[3] = vec4(p30.z, p31.z, p32.z, p33.z);

	tp = vvec * tb * pointsCoordMat * basis * uvec;
	vpos.z = tp.x + tp.y + tp.z + tp.w;
	tp = dvvec * tb * pointsCoordMat * basis * uvec;
	positionDV.z = tp.x + tp.y + tp.z + tp.w;
	tp = vvec * tb * pointsCoordMat * basis * duvec;
	positionDU.z = tp.x + tp.y + tp.z + tp.w;

	vpos.w = 1.0;

	gl_Position = mubo.PV * vpos;
	tcpos = mubo.model * gl_Position;


	// NORMALS

	tnormal = normalize(cross(positionDU, positionDV));
	*/

	vec2 is = vec2(500.0);

	float u = gl_TessCoord.x;
	float v = gl_TessCoord.y;

	vec3 lx = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
	vec3 ly = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;

	vec3 pos = 
	gl_in[0].gl_Position.xyz +
	lx * u + 
	ly * v;
	vec2 tpos = (rPos.xz + lx.xz * u + ly.xz * v) / is;
	pos.y = texture(hMap, tpos).x * 50.0;
	tnormal = (mubo.model * texture(nMap, tpos)).xyz;
	toUv = vec2(1.0) * vec2(u, v);

	gl_Position = mubo.PV * vec4(pos, 1.0);
	tcpos = vec4(pos, 1.0);
}

