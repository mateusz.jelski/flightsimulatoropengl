#version 420 core

in vec3 tnormal;
in vec4 tcpos;
in vec2 toUv;

uniform sampler2D terrain;

layout(std140, binding = 1) uniform LUBO
{
vec3 lightSource;
vec4 lightColor;
} lubo;

float lerpf(float a, float b, float v)
{
    return a + v * (b - a);
}

float lerpfc(float _min, float _max, float _value)
{
    return _min + (_max * _value);
}

vec2 N22(vec2 p)
{
    vec3 a = fract(p.xyx * vec3(123.34, 234.34, 345.65));
    a += dot(a, a + 34.45);
    return fract(vec2(a.x*a.y, a.y * a.z));
}

vec3 terrainTexture(vec2 uv)
{

    vec2 uvt = floor(uv * 456.0);

    vec3 mc_l = vec3(0.0, 0.6, 0.0);
    vec3 mc_d = vec3(0.0, 0.4, 0.0);
    vec3 mc_bl = vec3(165.0 / 255.0, 42.0 / 255.0, 52.0 / 255.0);
    vec3 mc_bd = vec3(0.5, 0.1, 0.0);
    float r = N22(uvt * 0.7).x;
    
    vec3 mc = mix(mc_l, mc_d, lerpfc(0.0, 0.5,r) * 1.1);
    r = N22(uvt * 1.2).x;
    
    mc = mix(mc, mc_bl, lerpfc(0.0, 0.6, min(0.35,r) * 1.3));
    
    r = N22(uvt * 0.35).x;
    mc = mix(mc, mc_bd, lerpfc(0.0, 0.3, min(0.8, r)));
    
    return mc;
}

void main()
{
    vec3 fogColor = vec3(0.6);
	vec2 res = vec2(1920.0, 1080.0);
    vec2 uv = gl_FragCoord.xy / res;

    vec3 object_color = texture(terrain, toUv).xyz;

	vec3 ld = normalize(lubo.lightSource - vec3(tcpos));
	float dif = max(dot(tnormal, ld), 0.05);

    float fogDist = step(0.98, 1.0 - gl_FragCoord.w)*smoothstep(0.98, 1.0,1.0 - gl_FragCoord.w );
    float lightDist = 1.0 / (1.0 + 0.12 * distance(lubo.lightSource, vec3(tcpos)));

    vec3 cc = object_color * (lubo.lightColor.xyz * ((dif * lightDist + 0.2)));
    cc.x = lerpf(cc.x, fogColor.x, fogDist * 0.75);
    cc.y = lerpf(cc.y, fogColor.y, fogDist * 0.75);
    cc.z = lerpf(cc.z, fogColor.z, fogDist * 0.75);
    
    gl_FragColor = vec4(cc, 1.0);
}