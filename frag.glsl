#version 420 core

in vec3 normal;
in vec4 cpos;
in vec2 oUv;

layout(std140, binding = 1) uniform LUBO
{
vec3 lightSource;
vec4 lightColor;
} lubo;

uniform sampler2D texture0;

void main()
{
    vec2 res = vec2(800.0, 800.0);
    vec2 uv = gl_FragCoord.xy / res;
    
    vec3 object_color = vec3(1.0);


	vec3 ld = normalize(lubo.lightSource - vec3(cpos));
	float dif = max(dot(normal, ld), 0.05);

	//gl_FragColor = vec4(object_color * vec3((lightColor * 0.2) + dif),1.0);
    //gl_FragColor = texture(texture0, oUv);
    gl_FragColor = vec4(object_color * vec3((lubo.lightColor * 0.2) + dif) * texture(texture0, oUv).xyz,1.0);
}