#include "Octree.h"

template<typename T>
inline OctreeNode<T>::OctreeNode()
{
	maskChildren = 0; maskObjects = 0;
}

template<typename T>
inline void OctreeNode<T>::setHMask(uchar mask)
{
	maskChildren = mask;
}

template<typename T>
void appendFront(OctreeLNode<T>** node, T data)
{
	*node = new OctreeLNode<T>{ data, *node };
}

template<typename T>
inline void OctreeNode<T>::setOMask(uchar mask)
{
	maskObjects = mask;
}

template<typename T>
inline bool OctreeNode<T>::maskChildrenAnd(uchar m)
{
	return maskChildren & m;
}

template<typename T>
inline bool OctreeNode<T>::maskObjectsAnd(uchar m)
{
	return maskObjects & m;
}

inline v3df boundPos(Boundf* b)
{
	return v3df{ b->x, b->y, b->z };
}

inline v3df boundSize(Boundf* b)
{
	return v3df{ b->w, b->h, b->d };
}

inline v4df bound4Size(Boundf* b)
{
	return v4df{ b->w, b->h, b->d, 0.0f };
}

template<typename T, std::uintmax_t max_depth>
Octree<T, max_depth>::Octree(v3df pos, v3df hsize)
{
	this->pos = pos;
	this->hsize = hsize;

	OCNode = new OctreeNode<T>();
	OCNode->setHMask(0xff);

	OCNode->children[0] = new OctreeNode<T>();
	OCNode->children[1] = new OctreeNode<T>();
	OCNode->children[2] = new OctreeNode<T>();
	OCNode->children[3] = new OctreeNode<T>();
	OCNode->children[4] = new OctreeNode<T>();
	OCNode->children[5] = new OctreeNode<T>();
	OCNode->children[6] = new OctreeNode<T>();
	OCNode->children[7] = new OctreeNode<T>();

	//OCObject = new Node<T>();

	OCTD = new OctreeTempData<T>{};
}

template<typename T, std::uintmax_t max_depth>
Octree<T, max_depth>::Octree()
{
}

template<typename T, std::uintmax_t max_depth>
inline bool __fastcall Octree<T, max_depth>::collideBoundfp(Boundf* tbound, Boundf* bound)
{
	return (bound->x <= tbound->x + tbound->w && bound->x + bound->w >= tbound->x) &&
		(bound->y <= tbound->y + tbound->h && bound->y + bound->h >= tbound->y) &&
		(bound->z <= tbound->z + tbound->d && bound->z + bound->d >= tbound->z);
}

template<typename T, std::uintmax_t max_depth>
inline bool Octree<T, max_depth>::vcollideBoundfp(Boundf* tbound, Boundf* bound)
{
	return false;
}

template<typename T, std::uintmax_t max_depth>
inline bool __fastcall Octree<T, max_depth>::collideBoundf(Boundf tbound, Boundf bound)
{
	return (bound.x <= tbound.x + tbound.w && bound.x + bound.w >= tbound.x) &&
		(bound.y <= tbound.y + tbound.h && bound.y + bound.h >= tbound.y) &&
		(bound.z <= tbound.z + tbound.d && bound.z + bound.d >= tbound.z);
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::insert(T obj, Boundf bound)
{
	dpth = 0;
	OCTD->obj = new OctreeObject<T>{ obj, bound };
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo B
	{																			//oo
		insertIns(OCNode->children[0]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox B
	{																			//oo
		insertIns(OCNode->children[1]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//ox
		insertIns(OCNode->children[2]);
		return;
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//xo
		insertIns(OCNode->children[3]);
		return;
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo F
	{																			//oo
		insertIns(OCNode->children[4]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox F
	{																			//oo
		insertIns(OCNode->children[5]);
		return;
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//ox
		insertIns(OCNode->children[6]);
		return;
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//xo
		insertIns(OCNode->children[7]);
		return;
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collide(Boundf bound)
{
	Boundf* bfp = &bound;
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, bfp))						//xo B
	{																			//oo
		collideIns(OCNode->children[0], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox B
	{																			//oo
		collideIns(OCNode->children[1], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//ox
		collideIns(OCNode->children[2], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//xo
		collideIns(OCNode->children[3], bfp, 1);
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//xo F
	{																			//oo
		collideIns(OCNode->children[4], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox F
	{																			//oo
		collideIns(OCNode->children[5], bfp, 1);
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//ox
		collideIns(OCNode->children[6], bfp, 1);
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//xo
		collideIns(OCNode->children[7], bfp, 1);
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::insertIns(OctreeNode<T>* node)
{
	dpth++;
	v3df pos = boundPos(&OCTD->bound);
	v3df hsize = boundSize(&OCTD->bound);
	hsize.x = hsize.x / 2.0f; hsize.y = hsize.y / 2.0f; hsize.z = hsize.z / 2.0f;
	//OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo B
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_BACK;
				node->children[0] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[0], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_BACK)) {
			insertIns(node->children[0]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
			{
				node->children[0] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_UPPER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_UPPER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[0];
				node->children[0] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[0]);
				OCTD->obj = tempObject;
				OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
				dpth = sdpth;
				insertIns(node->children[0]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox B
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				node->children[1] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[1], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_BACK)) {
			insertIns(node->children[1]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
			{
				node->children[1] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[1];
				node->children[1] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[1]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[1]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//ox
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_LOWER_BACK;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_BACK;
				node->children[2] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[2], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_BACK)) {
			insertIns(node->children[2]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
			{
				node->children[2] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_LOWER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_LOWER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[2];
				node->children[2] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[2]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[2]);
				return;
			}
		}
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo B
	{																			//xo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_LOWER_BACK;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_BACK;
				node->children[3] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[3], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_BACK)) {
			insertIns(node->children[3]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
			{
				node->children[3] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_BACK;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_LOWER_BACK;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_LOWER_BACK;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[3];
				node->children[3] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[3]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[3]);
				return;
			}
		}
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//xo F
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_UPPER_FRONT;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_FRONT;
				node->children[4] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[4], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_FRONT)) {
			insertIns(node->children[4]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT)) // mask 
			{
				node->children[4] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_UPPER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_UPPER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_UPPER_FRONT;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[4];
				node->children[4] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//ox F
	{																			//oo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_UPPER_FRONT;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_FRONT;
				node->children[5] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[5], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_FRONT)) {
			insertIns(node->children[5]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
			{
				node->children[5] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_UPPER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_UPPER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_UPPER_FRONT;
				tempObject = (OctreeObject<T>*)node->children[5];
				node->children[5] = new OctreeNode<T>();
				// save object pointer and call another insertIns for this object
				int sdpth = dpth;
				insertIns(node->children[5]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[5]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//ox
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_RIGHT_LOWER_FRONT;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_FRONT;
				node->children[6] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[6], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_FRONT)) {
			insertIns(node->children[6]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
			{
				node->children[6] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_RIGHT_LOWER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_RIGHT_LOWER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_RIGHT_LOWER_FRONT;
				// save object pointer and call another insertIns for this object
				tempObject = (OctreeObject<T>*)node->children[6];
				node->children[6] = new OctreeNode<T>();
				int sdpth = dpth;
				insertIns(node->children[6]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[6]);
				return;
			}
		}
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, &OCTD->obj->bound))						//oo F
	{																			//xo
		if (dpth == max_depth)
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
			{
				//node->maskChildren = node->maskChildren ^ MASK_LEFT_LOWER_FRONT;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_FRONT;
				node->children[7] = (OctreeNode<T>*)new OctreeLNode<OctreeObject<T>*>{ OCTD->obj, nullptr };
				return;
			}
			appendFront((OctreeLNode<OctreeObject<T>*>**) & node->children[7], OCTD->obj);
			return;
		}
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_FRONT)) {
			insertIns(node->children[7]);
			return;
		}
		else
		{
			if (!node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
			{
				node->children[7] = (OctreeNode<T>*)OCTD->obj;
				node->maskObjects = node->maskObjects | MASK_LEFT_LOWER_FRONT;
				return;
			}
			else
			{
				node->maskChildren = node->maskChildren | MASK_LEFT_LOWER_FRONT;
				node->maskObjects = node->maskObjects ^ MASK_LEFT_LOWER_FRONT;
				tempObject = (OctreeObject<T>*)node->children[7];
				node->children[7] = new OctreeNode<T>();
				// save object pointer and call another insertIns for this object
				int sdpth = dpth;
				insertIns(node->children[7]);
				OCTD->obj = tempObject;
				OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
				OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
				dpth = sdpth;
				insertIns(node->children[7]);
				return;
			}
		}
	}
}

template<typename T, std::uintmax_t max_depth>
void Octree<T, max_depth>::collideIns(OctreeNode<T>* node, Boundf* bfp, int dpth_)
{
	v3df pos = boundPos(&OCTD->bound);
	v3df hsize = boundSize(&OCTD->bound);
	hsize.x = hsize.x / 2.0f; hsize.y = hsize.y / 2.0f, hsize.z = hsize.z / 2.0f;
	OCTD->bound = Boundf{ pos.x, pos.y, pos.z, hsize.x, hsize.y, hsize.z };
	if (collideBoundfp(&OCTD->bound, bfp))						//xo B
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_BACK))
			collideIns(node->children[0], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[0];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[0])->bound, bfp))
						((OctreeObject<T>*)node->children[0])->object.render(); //TODO template function 

	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox B
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_BACK))
			collideIns(node->children[1], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[1];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[1])->bound, bfp))
						((OctreeObject<T>*)node->children[1])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_BACK))
			collideIns(node->children[2], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[2];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[2])->bound, bfp))
						((OctreeObject<T>*)node->children[2])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo B
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_BACK))
			collideIns(node->children[3], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_BACK))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[3];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[3])->bound, bfp))
						((OctreeObject<T>*)node->children[3])->object.render.render(); //TODO template function 
	}

	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//xo F
	{																			//oo
		if (node->maskChildrenAnd(MASK_LEFT_UPPER_FRONT))
			collideIns(node->children[4], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[4];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[4])->bound, bfp))
						((OctreeObject<T>*)node->children[4])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//ox F
	{																			//oo
		if (node->maskChildrenAnd(MASK_RIGHT_UPPER_FRONT))
			collideIns(node->children[5], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_UPPER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[5];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[5])->bound, bfp))
						((OctreeObject<T>*)node->children[5])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x + hsize.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//ox
		if (node->maskChildrenAnd(MASK_RIGHT_LOWER_FRONT))
			collideIns(node->children[6], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_RIGHT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[6];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[6])->bound, bfp))
						((OctreeObject<T>*)node->children[6])->object.render.render(); //TODO template function 
	}
	OCTD->bound.x = pos.x; OCTD->bound.y = pos.y + hsize.y; OCTD->bound.z = pos.z + hsize.z;
	OCTD->bound.w = hsize.x; OCTD->bound.h = hsize.y; OCTD->bound.d = hsize.z;
	if (collideBoundfp(&OCTD->bound, bfp))						//oo F
	{																			//xo
		if (node->maskChildrenAnd(MASK_LEFT_LOWER_FRONT))
			collideIns(node->children[7], bfp, dpth_ + 1);
		else
			if (node->maskObjectsAnd(MASK_LEFT_LOWER_FRONT))
				if (dpth_ == max_depth)
				{
					auto N = (OctreeLNode<OctreeObject<T>*>*)node->children[7];
					do
					{
						if (collideBoundfp(&N->data->bound, bfp))
							N->data->object.render.render();
						N = N->next;
					} while ((N != nullptr));
				}
				else
					if (collideBoundfp(&((OctreeObject<T>*)node->children[7])->bound, bfp))
						((OctreeObject<T>*)node->children[7])->object.render.render(); //TODO template function 

	}
}