#version 430 core

#define PI 3.141592653589793

layout (local_size_x = 1, local_size_y = 1) in;
layout (r32f, binding = 0) uniform image2D imgMap;

layout (std140, binding = 1) uniform IMG_STUFF
{
	ivec2 size;

	float freq;
	float persis;
	float amp;

} imgCtrl;

float rand(vec2 c){
	return fract(sin(dot(c.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
 
float noise(vec2 p, float freq ){
	float screenWidth = 500.0;
	float unit = screenWidth/freq;
	vec2 ij = floor(p/unit);
	vec2 xy = mod(p,unit)/unit;
	//xy = 3.*xy*xy-2.*xy*xy*xy;
	xy = .5*(1.-cos(PI*xy));
	float a = rand((ij+vec2(0.,0.)));
	float b = rand((ij+vec2(1.,0.)));
	float c = rand((ij+vec2(0.,1.)));
	float d = rand((ij+vec2(1.,1.)));
	float x1 = mix(a, b, xy.x);
	float x2 = mix(c, d, xy.x);
	return mix(x1, x2, xy.y);
}
 
float pNoise(vec2 p, int res){
	float persistance = imgCtrl.persis;
	float n = 0.;
	float normK = 0.;
	float f = imgCtrl.freq;
	float amp = imgCtrl.amp;
	int iCount = 0;
	for (int i = 0; i<8; i++){
		n+=amp*noise(p, f);
		f*=2.;
		normK+=amp;
		amp*=persistance;
		if (iCount == res) break;
		iCount++;
	}
	float nf = n/normK;
	return nf*nf*nf*nf;
}

void main()
{
	//vec2 p = vec2(gl_GlobalInvocationID.xy);
	imageStore(imgMap, ivec2(gl_GlobalInvocationID.xy),
	vec4(20.0 * pNoise(vec2(gl_GlobalInvocationID.xy), 500))
	);

}