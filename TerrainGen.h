#pragma once

extern int pseed;

#include <noise.h>
#include <noisegen.h>
#include <ext/matrix_transform.hpp>

float* generatePatchArray(float patchSize, float patchX, float patchY, int* s);
float* generateHeightMapRects(float rs, float amountx, float amounty, int* s);
