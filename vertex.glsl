#version 420 core

layout(location = 0) in vec3 pos;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 nvec;

out vec3 normal;
out vec4 cpos;
out vec2 oUv;

layout (std140, binding = 0) uniform MUBO
{
 mat4 model;
 mat4 PV;
 mat4 scale;
} mubo;

void main()
{
	cpos = mubo.model * mubo.scale * vec4(pos, 1.0);
	gl_Position = mubo.PV * cpos;
	normal = mat3(mubo.model) * nvec;
	oUv = uv;
}