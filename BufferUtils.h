#pragma once
#include <glew.h>
#include <gtc/matrix_transform.hpp>

template<typename vecType>
struct vector3d
{
	vecType x, y, z;
};

class Buffer
{
public:
	Buffer();
	void store(void* data, unsigned int size, GLenum target, GLenum usage);
	Buffer(void* data, unsigned int size, GLenum target, GLenum usage);
	void bind();
	unsigned int getId();

private:
	unsigned int id;
	GLenum target;
};

class vBuffer
{
public:
	vBuffer();
	void bind();
	void addAttrib(unsigned int size, GLenum type, unsigned int stride, unsigned int offset);
	void enableAttrib();
	void enableAttribId(unsigned int id);

private:
	unsigned int id;
	unsigned int attribAmount;
};

class Object3dVaoBuffered
{
public:
	Object3dVaoBuffered(vBuffer* vao, GLenum drawType, unsigned int amount);
	void draw(unsigned int offset);

private:
	vBuffer* vaoBf;
	GLenum type;
	unsigned int amount;
};

class Transform
{
private:
	float rotateX, rotateY, rotateZ;
	float moveX, moveY, moveZ;
	float scaleX, scaleY, scaleZ;
	bool rotate, move, scale;

public:
	Transform(bool rotate, bool scale, bool move);
	glm::mat4 getMatrix();
	void setMatrix(glm::mat4* matrix, bool reset);
	vector3d<float*> getRotate();
	vector3d<float*> getMove();
	vector3d<float*> getScale();
	void setRotate(float x, float y, float z);
	void setMove(float x, float y, float z);
	void setScale(float x, float y, float z);
	void appendMove(float x, float y, float z);
	float getMoveX();
	float getMoveY();
};


