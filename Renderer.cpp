#include "Renderer.h"

RenderTarget::RenderTarget(vBuffer* vao, Buffer* ebo, int ind, int capacity, Texture2D* tex = nullptr, TARGET_RENDER_TYPE type = TARGET_RENDER_INDICE)
{
	vb = vao;
	this->ebo = ebo;
	t = new std::vector<_ModelParams>(capacity);
	count = 0;
	indices = ind;
	this->tex = tex;
	this->type = type;
}

RenderTarget::RenderTarget(vBuffer* vao, Buffer* ebo, int ind, int capacity, TARGET_RENDER_TYPE type = TARGET_RENDER_INDICE)
{
	vb = vao;
	this->ebo = ebo;
	t = new std::vector<_ModelParams>(capacity);
	count = 0;
	indices = ind;
	this->tex = nullptr;
	this->type = type;

}

void RenderTarget::addNewObject(mat4 model)
{
	t->at(count) = _ModelParams{model, true, true};
	count += 1;
}

void RenderTarget::addNewObject()
{
	t->at(count) = _ModelParams{ mat4(1.0f), true, true };
	count += 1;
}

bool* RenderTarget::getRenderLocation(int index)
{
	return &t->at(index).render;
}

void RenderTarget::renderClear(int index, int set)
{
	t->at(index).render = set;
}

void RenderTarget::renderClearAll(int set)
{
	for (auto i = t->begin(); i != t->end(); i++)
	{
		i->render = set;
	}
}

mat4* RenderTarget::getModel(uint target)
{
	return &t->at(target).model;
}

void RenderTarget::setModel(uint target, mat4 m)
{
	t->at(target).model = m;
}

void RenderTarget::bindVao()
{
	vb->bind();
}

void RenderTarget::bindEbo()
{
	ebo->bind();
}

void RenderTarget::BindTex()
{
	if (tex != nullptr)
		tex->bind();
}

TARGET_RENDER_TYPE RenderTarget::getTargetType()
{
	return type;
}

void RenderTarget::switchTargetType(TARGET_RENDER_TYPE type)
{
	this->type = type;
}

int RenderTarget::getIndice()
{
	return indices;
}

int RenderTarget::getTargetCount()
{
	return count;
}

Renderer::Renderer(uint modelL)
{
	map = std::unordered_map<uint, RenderTarget*>();
	mL = modelL;
	tL = getTL();
	glUniform1i(tL, 0);
}

void Renderer::addTarget(uint id, RenderTarget* tg)
{
	if (map.find(id) == map.end())
	{
		tg->addNewObject();
		map.insert({ id, tg });
	}
	else
	{
		tg->addNewObject();
	}
}

void Renderer::removeTrget(uint id)
{
	map.erase(id);
}

void Renderer::render(uint id)
{
	if (map.find(id) != map.end())
	{
		RenderTarget* tg = map.at(id);
		if (tg->getTargetType() == TARGET_RENDER_INDICE)
		{
			tg->bindEbo();
			tg->bindVao();
			tg->BindTex();
			for (int i = 0; i < tg->count; i++)
			{
				if (tg->t->at(i).enabled == true && tg->t->at(i).render == true)
				{
					glUniformMatrix4fv(mL, 1, GL_FALSE, &tg->t->at(i).model[0].x);
					glDrawElements(GL_TRIANGLES, tg->getIndice(), GL_UNSIGNED_INT, NULL);
				}
			}
		}
		else if (tg->getTargetType() == TARGET_RENDER_ARRAY)
		{
			tg->bindVao();
			tg->BindTex();
			for (int i = 0; i < tg->count; i++)
			{
				if (tg->t->at(i).enabled == true && tg->t->at(i).render == true)
				{
					glUniformMatrix4fv(mL, 1, GL_FALSE, &tg->t->at(i).model[0].x);
					glDrawArrays(GL_TRIANGLES, 0, tg->getIndice());
				}
			}
		}
	}
}

void Renderer::renderTarget(uint id, uint tg)
{
	RenderTarget* tgf = map.at(id);
	tgf->bindEbo();
	tgf->bindVao();
	tgf->BindTex();
	glUniformMatrix4fv(mL, 1, GL_FALSE, &tgf->t->at(tg).model[0].x);
	glDrawElements(GL_TRIANGLES, tgf->getIndice(), GL_UNSIGNED_INT, NULL);
}

mat4* Renderer::targetMatrix(uint id, uint tgi)
{
	return getTarget(id)->getModel(tgi);
}

void Renderer::targetSetMatrix(uint id, uint tgi, mat4 m)
{
	getTarget(id)->setModel(tgi, m);
}

void Renderer::renderCustomBind(uint id, std::function<void(void*, void*)> f)
{
	if (map.find(id) != map.end())
	{
		RenderTarget* tg = map.at(id);
		if (tg->getTargetType() == TARGET_RENDER_INDICE)
		{
			tg->bindEbo();
			tg->bindVao();
			tg->BindTex();
			for (int i = 0; i < tg->count; i++)
			{
				if (tg->t->at(i).enabled == true && tg->t->at(i).render == true)
				{
					f((void*)&tg->t->at(i).model, (void*)&mL);
					glDrawElements(GL_TRIANGLES, tg->getIndice(), GL_UNSIGNED_INT, NULL);
				}
			}
		}
		else if (tg->getTargetType() == TARGET_RENDER_ARRAY)
		{
			tg->bindVao();
			tg->BindTex();
			for (int i = 0; i < tg->count; i++)
			{
				if (tg->t->at(i).enabled == true && tg->t->at(i).render == true)
				{
					f((void*)&tg->t->at(i).model, (void*)&mL);
					glDrawArrays(GL_TRIANGLES, 0, tg->getIndice());
				}
			}
		}

	}
}

void Renderer::resizeTargetSpaceTo(uint tg, size_t capacity)
{
	getTarget(tg)->t->reserve(capacity);
}

void Renderer::targetEnable(uint tg, uint tgi)
{
	getTarget(tg)->t->at(tgi).enabled = true;
}

void Renderer::targetDisable(uint tg, uint tgi)
{
	getTarget(tg)->t->at(tgi).enabled = false;
}

bool Renderer::targetGetState(uint tg, uint tgi)
{
	return getTarget(tg)->t->at(tgi).enabled;
}

RenderTarget* Renderer::getTarget(uint id)
{
	return map.at(id);
}

void Renderer::setTL(uint tL)
{
	this->tL = tL;
}

void Renderer::renderClearAll(int set)
{
	for (auto i = map.begin(); i != map.end(); i++)
	{
		i->second->renderClearAll(set);
	}
}

void Renderer::renderClearTragetAll(int set, int target)
{
	map.find(target)->second->renderClearAll(set);
}

void Renderer::renderClearTargetSelect(int set, int target, int index)
{
	map.find(target)->second->renderClear(index, set);
}

void Renderer::bindTexture(uint id)
{
	glUniform1i(tL, id);
}

uint Renderer::getTL()
{
	int t;
	glGetIntegerv(GL_CURRENT_PROGRAM, &t);
	return glGetUniformLocation(t, "texture0");
}

Texture2D::Texture2D(const char* path, GLenum mode, GLenum wrap)
{
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	data = read(path, &x, &y, &chn);

	if (chn == 3)
		type = GL_RGB;
	else if (chn == 4)
		type = GL_RGBA;
	glTexImage2D(GL_TEXTURE_2D, 0, mode, x, y, 0, type, GL_UNSIGNED_BYTE, data);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	type = mode;

	glGenerateMipmap(GL_TEXTURE_2D);
}

Texture2D::Texture2D(GLenum type, GLenum wrap, int x, int y)
{
	glGenTextures(1, &id);
	glBindTexture(GL_TEXTURE_2D, id);
	this->x = x;
	this->y = y;
	data = nullptr;

	glTexImage2D(GL_TEXTURE_2D, 0, type, x, y, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	this->type = type;

	//glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture2D::bind()
{
	glBindTexture(GL_TEXTURE_2D, id);
}

void Texture2D::unbind()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture2D::bindImage(uint texIndex, GLenum acces)
{
	glBindImageTexture(texIndex, id, 0, GL_FALSE, 0, acces, type);
}

void Texture2D::mipmapgen()
{
	glGenerateMipmap(GL_TEXTURE_2D);
}

void Texture2D::clear()
{
	delete[] data;
}

void Texture2D::clearBuffer()
{
	glDeleteTextures(1, &id);
}

char* Texture2D::sample(int _x, int _y)
{
	return data + ((_x + (_y * x)) * chn);
}

uint compileShader(GLenum type, const char* src)
{
	uint shdr = glCreateShader(type);
	glShaderSource(shdr, 1, &src, NULL);
	glCompileShader(shdr);
	return shdr;
}

uint compileProgram(uint vs, uint fs)
{
	uint prgm = glCreateProgram();
	glAttachShader(prgm, vs);
	glAttachShader(prgm, fs);
	glLinkProgram(prgm);
	return prgm;
}

uint compileProgram(uint vs, uint gs, uint fs)
{
	uint prgm = glCreateProgram();
	glAttachShader(prgm, vs);
	glAttachShader(prgm, fs);
	glAttachShader(prgm, gs);
	glLinkProgram(prgm);
	return prgm;
}

void dbgShaderDebugMsg(uint shader, char* bf, uint bfSize)
{
	int c;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &c);
	if (c == 0)
		glGetShaderInfoLog(shader, bfSize, &c, bf);
}

vBuffer* vertexStoreArray(std::vector<float>* v, Buffer* vbo, ATTRIB_TYPE_STORE type)
{
	vBuffer* vao = new vBuffer();
	vbo->store(&v->front(), v->size() * 4, GL_ARRAY_BUFFER, GL_STATIC_DRAW);

	switch(type)
	{
	case STORE_ATTRIB_VERTEX_TEXTURE_NORMAL:
		vao->addAttrib(3, GL_FLOAT, 8 * 4, 0);
		vao->addAttrib(2, GL_FLOAT, 8 * 4, 12);
		vao->addAttrib(3, GL_FLOAT, 8 * 4, 20);
		vao->enableAttribId(0);
		vao->enableAttribId(1);
		vao->enableAttribId(2);
		break;
	case STORE_ATTRIB_VERTEX_NORMAL:
		vao->addAttrib(1, GL_FLOAT, 6 * 4, 0);
		vao->addAttrib(3, GL_FLOAT, 6 * 4, 12);
		vao->enableAttribId(1);
		vao->enableAttribId(3);
		break;
	case STORE_ATTRIB_VERTEX:
		vao->addAttrib(1, GL_FLOAT, 3 * 4, 0);
		vao->enableAttribId(1);
		break;
	default:
		throw "No store function";
	}
	return vao;
	
}

void texUnitActivate(GLenum e)
{
	glActiveTexture(e);
}

UniformBuffer::UniformBuffer(uint size, GLenum mode, uint bindTo)
{
	glGenBuffers(1, &id);
	glBindBuffer(GL_UNIFORM_BUFFER, id);

	glBufferData(GL_UNIFORM_BUFFER, size, NULL, mode);	
	bind_ = bindTo;
}

void UniformBuffer::bind()
{
	glBindBuffer(GL_UNIFORM_BUFFER, id);
}

void UniformBuffer::bindBase()
{
	glBindBufferBase(GL_UNIFORM_BUFFER, bind_, id);
}

void UniformBuffer::subData(uint offset, uint size, void* d)
{
	glBufferSubData(GL_UNIFORM_BUFFER, offset, size, d);
}

void UniformBuffer::unbind()
{
	glBindBuffer(GL_UNIFORM_BUFFER, 0);
}

void UniformBuffer::bindBaseTo(uint bp)
{
	glBindBufferBase(GL_UNIFORM_BUFFER, bp, id);
}

uint UniformBuffer::getBindingPoint()
{
	return bind_;
}

void UniformBuffer::setBindingPoint(uint p)
{
	bind_ = p;
}

uint UniformBuffer::getID()
{
	return id;
}

void UniformBuffer::store(uint size, void* d)
{
	glBufferSubData(GL_UNIFORM_BUFFER, 0, size, d);
	bindBase();
}
