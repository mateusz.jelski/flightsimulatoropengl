#include "ObjLoader.h"

using namespace std;

void indexLoadToVector(std::vector<float>* vec, std::string* str, int startAt, float* v, float* vt, float* vn, int* fi)
{
	char bfValue[16];
	char bfCounter = 0;
	int indexes[3];
	char indCounter = 0;
	memset(bfValue, 0, 16);
	char cval = 0;

	if (fi[0] != 0)
	{
		readIndex3(fi[0] - 1, v, vec);
		readIndex2(fi[1] - 1, vt, vec);
		readIndex3(fi[2] - 1, vn, vec);
		cval++;
	}

	int startAtNext = 0;

	bool endwrite = true;

	for (int i = startAt+1; i < str->size(); i++)
	{
		if (str->at(i) == ' ')
		{
			indexes[indCounter] = atoi(bfValue);
			if (fi[0] == 0)
			{
				fi[0] = indexes[0];
				fi[1] = indexes[1];
				fi[2] = indexes[2];
			}
			if (cval == 2)
			{
				readIndex3(indexes[0] - 1, v, vec);
				readIndex2(indexes[1] - 1, vt, vec);
				readIndex3(indexes[2] - 1, vn, vec);
				indexLoadToVector(vec, str, startAtNext, v, vt, vn, fi);
				endwrite = false;
				break;
			}
			cval++;
			
			memset(bfValue, 0, 16);
			indCounter = 0;
			bfCounter = 0;

			readIndex3(indexes[0] - 1, v, vec);
			readIndex2(indexes[1] - 1, vt, vec);
			readIndex3(indexes[2] - 1, vn, vec);
			memset(indexes, 0, 3);
			startAtNext = i;
		}
		else if (str->at(i) == '/')
		{
			indexes[indCounter] = atoi(bfValue);
			memset(bfValue, 0, 16);
			indCounter++;
			bfCounter = 0;
		}
		else
		{
			bfValue[bfCounter] = str->at(i);
			bfCounter++;
		}

	}
	if (endwrite == true)
	{
		indexes[indCounter] = atoi(bfValue);
		readIndex3(indexes[0] - 1, v, vec);
		readIndex2(indexes[1] - 1, vt, vec);
		readIndex3(indexes[2] - 1, vn, vec);
	}
}

void readFaces3(char bf[16], int* inds)
{
	char bfIndex = 0;
	char bfCounter = 0;
	char ccsCounter = 0;
	char ccs[16];
	memset(ccs, '\x00', 16);

	for (int i = 0; i < 16; i++)
	{
		if (bf[bfCounter] == '/')
		{
			inds[bfIndex] = atoi(ccs);
			bfIndex++;
			bfCounter++;
			ccsCounter = 0;
			memset(ccs, '\x00', 16);
		}
		else
		{
			ccs[ccsCounter] = bf[bfCounter];
			bfCounter++;
			ccsCounter++;
		}
	}
	inds[bfIndex] = atoi(ccs);
}

void readFloat3(char bf[3][16], string str, int sp)
{
	char bfIndex = 0;
	char bfCounter = 0;

	for (int i = sp+1; i < str.size(); i++)
	{
		if (str.at(i) == ' ')
		{
			bfIndex++;
			bfCounter = 0;
		}
		else
		{
			bf[bfIndex][bfCounter] = str.at(i);
			bfCounter++;
		}
		if (bfIndex > 2)
			break;
	}
}

void loadObjToList(vector<float>* targetArray, char* path, uint bfSize)
{
	char type[8];
	int fb[3];
	int i;

	char floatsBf[3][16];

	float* v = new float[bfSize];
	float* vt = new float[bfSize];
	float* vn = new float[bfSize];

	int* fi_ = new int[3];
	fi_[0] = 0;

	int vi = 0, vti = 0, vni = 0;
	int fi;

	if (targetArray->capacity() < 1)
		targetArray->reserve(128);

	string line;
	ifstream st(path); 

	while (getline(st, line))
	{
		memset(type, '\x00', 8);
		memset(floatsBf[0], '\x00', 16);
		memset(floatsBf[1], '\x00', 16);
		memset(floatsBf[2], '\x00', 16);
		for (i = 0; i < line.size(); i++)
		{
			if (line.at(i) == '#')
				break;
			if (line.at(i) == ' ')
				break;
			type[i] = line.at(i);
		}

		if (memcmp(type, "v", 1) == 0)
		{ 
			readFloat3(floatsBf, line, i);
			v[vi] = (float)atof(floatsBf[0]);
			v[vi+1] = (float)atof(floatsBf[1]);
			v[vi+2] = (float)atof(floatsBf[2]);
			vi += 3;
		}
		if (memcmp(type, "vt", 2) == 0)
		{ 
			readFloat3(floatsBf, line, i);
			vt[vti] = (float)atof(floatsBf[0]);
			vt[vti + 1] = (float)atof(floatsBf[1]);
			//vt[vti + 2] = (float)atof(floatsBf[2]);
			vti += 2;
		}
		if (memcmp(type, "vn", 2) == 0)
		{ 
			readFloat3(floatsBf, line, i);
			vn[vni] = (float)atof(floatsBf[0]);
			vn[vni + 1] = (float)atof(floatsBf[1]);
			vn[vni + 2] = (float)atof(floatsBf[2]);
			vni += 3;
		}
		if (memcmp(type, "f", 2) == 0)
		{
			indexLoadToVector(targetArray, &line, i, v, vt, vn, fi_);
			fi_[0] = 0;
		}
	}
	delete[] v;
	delete[] vt;
	delete[] vn;
}

void readIndex3(uint index, float* a, std::vector<float>* vf)
{
	index = index * 3;
	vf->push_back(a[index]);
	vf->push_back(a[index+1]);
	vf->push_back(a[index+2]);
}

void readIndex2(uint index, float* a, std::vector<float>* vf)
{
	index = index * 2;
	vf->push_back(a[index]);
	vf->push_back(a[index + 1]);
}

void freeObjMemory(std::vector<float>* v)
{
	v->clear();
	delete v;
}

void freeObjMemoryFill(std::vector<float>* v)
{
	v->clear();
}

using namespace glm;

void getObjectCenter(glm::vec3* min, glm::vec3* max, std::vector<float>* arr, int PACKED_DATA)
{
	vec3 vmin, vmax;
	vmin = vec3(-INFINITY);

	for (auto i = arr->begin(); i != arr->end(); i += PACKED_DATA)
	{
		
	}
}
